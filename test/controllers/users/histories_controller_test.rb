require "test_helper"

class Users::HistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_history = users_histories(:one)
  end

  test "should get index" do
    get users_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_users_history_url
    assert_response :success
  end

  test "should create users_history" do
    assert_difference("Users::History.count") do
      post users_histories_url, params: { users_history: { histories_name: @users_history.histories_name, histories_status: @users_history.histories_status } }
    end

    assert_redirected_to users_history_url(Users::History.last)
  end

  test "should show users_history" do
    get users_history_url(@users_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_history_url(@users_history)
    assert_response :success
  end

  test "should update users_history" do
    patch users_history_url(@users_history), params: { users_history: { histories_name: @users_history.histories_name, histories_status: @users_history.histories_status } }
    assert_redirected_to users_history_url(@users_history)
  end

  test "should destroy users_history" do
    assert_difference("Users::History.count", -1) do
      delete users_history_url(@users_history)
    end

    assert_redirected_to users_histories_url
  end
end
