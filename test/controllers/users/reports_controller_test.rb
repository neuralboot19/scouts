require "test_helper"

class Users::ReportsControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get users_reports_index_url
    assert_response :success
  end
end
