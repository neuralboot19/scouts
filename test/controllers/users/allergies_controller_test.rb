require "test_helper"

class Users::AllergiesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_allergy = users_allergies(:one)
  end

  test "should get index" do
    get users_allergies_url
    assert_response :success
  end

  test "should get new" do
    get new_users_allergy_url
    assert_response :success
  end

  test "should create users_allergy" do
    assert_difference("Users::Allergy.count") do
      post users_allergies_url, params: { users_allergy: { allergies_name: @users_allergy.allergies_name, allergies_status: @users_allergy.allergies_status } }
    end

    assert_redirected_to users_allergy_url(Users::Allergy.last)
  end

  test "should show users_allergy" do
    get users_allergy_url(@users_allergy)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_allergy_url(@users_allergy)
    assert_response :success
  end

  test "should update users_allergy" do
    patch users_allergy_url(@users_allergy), params: { users_allergy: { allergies_name: @users_allergy.allergies_name, allergies_status: @users_allergy.allergies_status } }
    assert_redirected_to users_allergy_url(@users_allergy)
  end

  test "should destroy users_allergy" do
    assert_difference("Users::Allergy.count", -1) do
      delete users_allergy_url(@users_allergy)
    end

    assert_redirected_to users_allergies_url
  end
end
