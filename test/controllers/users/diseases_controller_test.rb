require "test_helper"

class Users::DiseasesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_disease = users_diseases(:one)
  end

  test "should get index" do
    get users_diseases_url
    assert_response :success
  end

  test "should get new" do
    get new_users_disease_url
    assert_response :success
  end

  test "should create users_disease" do
    assert_difference("Users::Disease.count") do
      post users_diseases_url, params: { users_disease: { diseases_name: @users_disease.diseases_name, diseases_status: @users_disease.diseases_status } }
    end

    assert_redirected_to users_disease_url(Users::Disease.last)
  end

  test "should show users_disease" do
    get users_disease_url(@users_disease)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_disease_url(@users_disease)
    assert_response :success
  end

  test "should update users_disease" do
    patch users_disease_url(@users_disease), params: { users_disease: { diseases_name: @users_disease.diseases_name, diseases_status: @users_disease.diseases_status } }
    assert_redirected_to users_disease_url(@users_disease)
  end

  test "should destroy users_disease" do
    assert_difference("Users::Disease.count", -1) do
      delete users_disease_url(@users_disease)
    end

    assert_redirected_to users_diseases_url
  end
end
