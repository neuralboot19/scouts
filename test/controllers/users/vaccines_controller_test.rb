require "test_helper"

class Users::VaccinesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_vaccine = users_vaccines(:one)
  end

  test "should get index" do
    get users_vaccines_url
    assert_response :success
  end

  test "should get new" do
    get new_users_vaccine_url
    assert_response :success
  end

  test "should create users_vaccine" do
    assert_difference("Users::Vaccine.count") do
      post users_vaccines_url, params: { users_vaccine: { vaccine_name: @users_vaccine.vaccine_name, vaccine_status: @users_vaccine.vaccine_status } }
    end

    assert_redirected_to users_vaccine_url(Users::Vaccine.last)
  end

  test "should show users_vaccine" do
    get users_vaccine_url(@users_vaccine)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_vaccine_url(@users_vaccine)
    assert_response :success
  end

  test "should update users_vaccine" do
    patch users_vaccine_url(@users_vaccine), params: { users_vaccine: { vaccine_name: @users_vaccine.vaccine_name, vaccine_status: @users_vaccine.vaccine_status } }
    assert_redirected_to users_vaccine_url(@users_vaccine)
  end

  test "should destroy users_vaccine" do
    assert_difference("Users::Vaccine.count", -1) do
      delete users_vaccine_url(@users_vaccine)
    end

    assert_redirected_to users_vaccines_url
  end
end
