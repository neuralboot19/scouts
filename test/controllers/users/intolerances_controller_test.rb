require "test_helper"

class Users::IntolerancesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_intolerance = users_intolerances(:one)
  end

  test "should get index" do
    get users_intolerances_url
    assert_response :success
  end

  test "should get new" do
    get new_users_intolerance_url
    assert_response :success
  end

  test "should create users_intolerance" do
    assert_difference("Users::Intolerance.count") do
      post users_intolerances_url, params: { users_intolerance: { intolerances_name: @users_intolerance.intolerances_name, intolerances_status: @users_intolerance.intolerances_status } }
    end

    assert_redirected_to users_intolerance_url(Users::Intolerance.last)
  end

  test "should show users_intolerance" do
    get users_intolerance_url(@users_intolerance)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_intolerance_url(@users_intolerance)
    assert_response :success
  end

  test "should update users_intolerance" do
    patch users_intolerance_url(@users_intolerance), params: { users_intolerance: { intolerances_name: @users_intolerance.intolerances_name, intolerances_status: @users_intolerance.intolerances_status } }
    assert_redirected_to users_intolerance_url(@users_intolerance)
  end

  test "should destroy users_intolerance" do
    assert_difference("Users::Intolerance.count", -1) do
      delete users_intolerance_url(@users_intolerance)
    end

    assert_redirected_to users_intolerances_url
  end
end
