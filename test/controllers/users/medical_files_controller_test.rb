require "test_helper"

class Users::MedicalFilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_medical_file = users_medical_files(:one)
  end

  test "should get index" do
    get users_medical_files_url
    assert_response :success
  end

  test "should get new" do
    get new_users_medical_file_url
    assert_response :success
  end

  test "should create users_medical_file" do
    assert_difference("Users::MedicalFile.count") do
      post users_medical_files_url, params: { users_medical_file: { blood_type: @users_medical_file.blood_type, emergency_first_name: @users_medical_file.emergency_first_name, emergency_last_name: @users_medical_file.emergency_last_name, emergency_number: @users_medical_file.emergency_number, height: @users_medical_file.height, insurance_name: @users_medical_file.insurance_name, insurance_number: @users_medical_file.insurance_number, weight: @users_medical_file.weight } }
    end

    assert_redirected_to users_medical_file_url(Users::MedicalFile.last)
  end

  test "should show users_medical_file" do
    get users_medical_file_url(@users_medical_file)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_medical_file_url(@users_medical_file)
    assert_response :success
  end

  test "should update users_medical_file" do
    patch users_medical_file_url(@users_medical_file), params: { users_medical_file: { blood_type: @users_medical_file.blood_type, emergency_first_name: @users_medical_file.emergency_first_name, emergency_last_name: @users_medical_file.emergency_last_name, emergency_number: @users_medical_file.emergency_number, height: @users_medical_file.height, insurance_name: @users_medical_file.insurance_name, insurance_number: @users_medical_file.insurance_number, weight: @users_medical_file.weight } }
    assert_redirected_to users_medical_file_url(@users_medical_file)
  end

  test "should destroy users_medical_file" do
    assert_difference("Users::MedicalFile.count", -1) do
      delete users_medical_file_url(@users_medical_file)
    end

    assert_redirected_to users_medical_files_url
  end
end
