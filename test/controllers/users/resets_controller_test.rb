require "test_helper"

class Users::ResetsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_reset = users_resets(:one)
  end

  test "should get index" do
    get users_resets_url
    assert_response :success
  end

  test "should get new" do
    get new_users_reset_url
    assert_response :success
  end

  test "should create users_reset" do
    assert_difference("Users::Reset.count") do
      post users_resets_url, params: { users_reset: {  } }
    end

    assert_redirected_to users_reset_url(Users::Reset.last)
  end

  test "should show users_reset" do
    get users_reset_url(@users_reset)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_reset_url(@users_reset)
    assert_response :success
  end

  test "should update users_reset" do
    patch users_reset_url(@users_reset), params: { users_reset: {  } }
    assert_redirected_to users_reset_url(@users_reset)
  end

  test "should destroy users_reset" do
    assert_difference("Users::Reset.count", -1) do
      delete users_reset_url(@users_reset)
    end

    assert_redirected_to users_resets_url
  end
end
