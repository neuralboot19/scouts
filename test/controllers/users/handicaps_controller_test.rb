require "test_helper"

class Users::HandicapsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @users_handicap = users_handicaps(:one)
  end

  test "should get index" do
    get users_handicaps_url
    assert_response :success
  end

  test "should get new" do
    get new_users_handicap_url
    assert_response :success
  end

  test "should create users_handicap" do
    assert_difference("Users::Handicap.count") do
      post users_handicaps_url, params: { users_handicap: { handicaps_name: @users_handicap.handicaps_name, handicaps_status: @users_handicap.handicaps_status } }
    end

    assert_redirected_to users_handicap_url(Users::Handicap.last)
  end

  test "should show users_handicap" do
    get users_handicap_url(@users_handicap)
    assert_response :success
  end

  test "should get edit" do
    get edit_users_handicap_url(@users_handicap)
    assert_response :success
  end

  test "should update users_handicap" do
    patch users_handicap_url(@users_handicap), params: { users_handicap: { handicaps_name: @users_handicap.handicaps_name, handicaps_status: @users_handicap.handicaps_status } }
    assert_redirected_to users_handicap_url(@users_handicap)
  end

  test "should destroy users_handicap" do
    assert_difference("Users::Handicap.count", -1) do
      delete users_handicap_url(@users_handicap)
    end

    assert_redirected_to users_handicaps_url
  end
end
