require "test_helper"

class Api::V1::HomeControllerTest < ActionDispatch::IntegrationTest
  test "should get Index" do
    get api_v1_home_Index_url
    assert_response :success
  end
end
