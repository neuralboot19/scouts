require_relative 'custom_seeds/roles_seeder'
require_relative 'custom_seeds/users_seeder'
require_relative 'custom_seeds/permissions_admin_seeder'
require_relative 'custom_seeds/permissions_user_seeder'

roles = RolesSeeder.seed
role_admin = nil
role_user = nil

roles.each do |role|
  role_admin = role if role.rol_name == 'Admin'
  role_user = role if role.rol_name == 'User'
end

Amount.find_or_create_by amount_usd: 18.00, amount_exchange: 36.27, amount_status: true, amount_name: 'Pago Movil', amount_type: 0
Amount.find_or_create_by amount_usd: 25.00, amount_exchange: 0.93, amount_status: true, amount_name: 'Pago TDC', amount_type: 1
Amount.find_or_create_by amount_usd: 17.00, amount_exchange: 0.73, amount_status: true, amount_name: 'Pago Manual', amount_type: 2
Amount.find_or_create_by amount_usd: 12.00, amount_exchange: 3.73, amount_status: false, amount_name: 'Pago Promoción', amount_type: 1

Jobtitle.find_or_create_by jobtitle_name: 'Administración'
Jobtitle.find_or_create_by jobtitle_name: 'Contabilidad'
Jobtitle.find_or_create_by jobtitle_name: 'Finanzas'

CorporateJobtitle.find_or_create_by corporate_jobtitle_name: 'JEFE'
CorporateJobtitle.find_or_create_by corporate_jobtitle_name: 'GERENTE'
CorporateJobtitle.find_or_create_by corporate_jobtitle_name: 'COORDINADOR'

country = Country.find_or_create_by country_name: 'España', country_type: 1
country_two = Country.find_or_create_by country_name: 'Venezuela', country_type: 0, country_default: true
region_one = Region.find_or_create_by country_id: country_two.id, region_name: 'Trujillo', region_status: 0
region_two = Region.find_or_create_by country_id: country_two.id, region_name: 'Zulia', region_status: 0
district_one = District.find_or_create_by region_id: region_one.id, district_name: 'Valera', district_status: 0
district_two = District.find_or_create_by region_id: region_two.id, district_name: 'Perija', district_status: 0

# Crear grupos
group_001_admin = Group.find_or_create_by(district_id: district_one.id, group_name: 'Cristobal Mendoza', group_status: 0)
group_002_admin = Group.find_or_create_by(district_id: district_one.id, group_name: 'Flor de lis', group_status: 0)
group_001_user = Group.find_or_create_by(district_id: district_two.id, group_name: 'Santa Teresita', group_status: 0)

# Crear unidades
unit_ve_001 = Unit.find_or_create_by(unit_name: 'Manada', unit_gender: 'Femenino')
unit_ve_002 = Unit.find_or_create_by(unit_name: 'Tropa', unit_gender: 'Masculino')
unit_ve_003 = Unit.find_or_create_by(unit_name: 'Clan', unit_gender: 'Ambos')

# Establecer relaciones entre unidades y grupos a través de GroupUnit
group_unit_admin_001 = GroupUnit.find_or_create_by(unit: unit_ve_001, group: group_001_admin)
group_unit_admin_002 = GroupUnit.find_or_create_by(unit: unit_ve_002, group: group_002_admin)
group_unit_user_001 = GroupUnit.find_or_create_by(unit: unit_ve_003, group: group_001_user)

# Creamos los avances que perteneces a cada unidad
Advance.find_or_create_by unit_id: unit_ve_001.id, advance_name: 'Cachorro', secuencial: 1
Advance.find_or_create_by unit_id: unit_ve_001.id, advance_name: 'Huella fresca', secuencial: 2
Advance.find_or_create_by unit_id: unit_ve_002.id, advance_name: 'Explorador', secuencial: 1
Advance.find_or_create_by unit_id: unit_ve_002.id, advance_name: 'Aventurero', secuencial: 2
Advance.find_or_create_by unit_id: unit_ve_003.id, advance_name: 'Precursor', secuencial: 1
Advance.find_or_create_by unit_id: unit_ve_003.id, advance_name: 'Juanito', secuencial: 2

state_e = State.find_or_create_by country_id: country.id, state_name: 'Madrid'
state_v = State.find_or_create_by country_id: country_two.id, state_name: 'Trujillo'

province_e_01 = Province.find_or_create_by state_id: state_e.id, province_name: 'Madrid'
province_v_01 = Province.find_or_create_by state_id: state_v.id, province_name: 'Valera'

Parish.find_or_create_by province_id: province_e_01.id, parishe_name: 'Madrid'
Parish.find_or_create_by province_id: province_v_01.id, parishe_name: 'San Luis'
Parish.find_or_create_by province_id: province_v_01.id, parishe_name: 'La beatriz'

birthdate = DateTime.now - 13404

# miembro institucional con grupo mayor de edad
user_member = Member.find_or_create_by cedula: '12345678', gender: 'Femenino', first_name: 'Maria', middle_name: 'Ana',
                                       last_name: 'Giron', birthdate:, age: 18, member_type: 2, second_email: 'maria@gmail.com',
                                       group_unit: group_unit_admin_001, citizenship: 1
# miembro institucional sin grupo mayor de edad
user_member1 = Member.find_or_create_by cedula: '17593374', gender: 'Masculino', first_name: 'Cesar', middle_name: 'Miguel',
                                        birthdate:, age: 20, member_type: 2, second_email: 'cesar602@gmail.com'

# miembro no institucional con grupo mayor de edad
user_member2 = Member.find_or_create_by cedula: '13241213', gender: 'Femenino', first_name: 'Lizet', middle_name: 'Alejandra',
                                        last_name: 'Gutierrez', birthdate:, age: 20, member_type: 1, second_email: 'alejandra@gmail.com',
                                        group_unit: group_unit_admin_002, citizenship: 1

# miembro no institucional sin grupo mayor de edad
user_member3 = Member.find_or_create_by cedula: '32132413', gender: 'Masculino', first_name: 'Carlos', middle_name: 'Stiven',
                                        last_name: 'Pelaez', birthdate:, age: 20, member_type: 1, second_email: 'stiven@gmail.com'

# miembro no institucional sin grupo menor de edad
user_member4 = Member.find_or_create_by cedula: '32413211', gender: 'Femenino', first_name: 'Milan', middle_name: 'Alexa',
                                        last_name: 'Pelaez', birthdate:, age: 16, member_type: 1, second_email: 'milan@gmail.com'

clave = '123456'
UsersSeeder.seed('admin@admin.com', clave, role_admin)
UsersSeeder.seed('admin1@admin.com', clave, role_admin)
UsersSeeder.seed('admin2@admin.com', clave, role_admin)
UsersSeeder.seed('user@user.com', clave, role_user, user_member)
UsersSeeder.seed('user1@user.com', clave, role_user, user_member1)
UsersSeeder.seed('user2@user.com', clave, role_user, user_member2)
UsersSeeder.seed('user3@user.com', clave, role_user, user_member3)
UsersSeeder.seed('user4@user.com', clave, role_user, user_member4)

%w[Catolica Evangelica Cristiana Musulman Ateo].each do |religion|
  Religion.find_or_create_by religion_name: religion
end

permissionsadmin = PermissionsAdminSeeder.seed
permissionsuser = PermissionsUserSeeder.seed

role_admin.role_permissions.create permission: permissionsadmin
permissionsuser.each do |permission|
  role_user.role_permissions.create permission:
end
