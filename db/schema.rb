# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2024_06_03_131110) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "advance_courses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "advance_id"
    t.uuid "course_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "advances", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "unit_id"
    t.string "advance_name"
    t.integer "secuencial"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "allergies", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "allergies_name"
    t.boolean "allergies_status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "allergy_members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id", null: false
    t.uuid "allergy_id", null: false
    t.boolean "allergy_m_status", default: false
    t.string "allergy_m_description"
    t.datetime "allergy_m_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "amounts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "amount_name"
    t.decimal "amount_usd", precision: 10, scale: 2
    t.decimal "amount_exchange", precision: 10, scale: 2
    t.boolean "amount_status"
    t.integer "amount_type", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "api_settings", force: :cascade do |t|
    t.string "api_url"
    t.string "api_token"
    t.string "api_token_refresh"
    t.string "api_password"
    t.string "api_dni"
    t.string "expire_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "corporate_jobtitles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "corporate_jobtitle_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "corporate_members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id"
    t.uuid "corporate_jobtitle_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "region_id"
    t.uuid "district_id"
    t.index ["district_id"], name: "index_corporate_members_on_district_id"
    t.index ["region_id"], name: "index_corporate_members_on_region_id"
  end

  create_table "countries", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "country_name"
    t.integer "country_type", default: 0
    t.boolean "archived", default: false
    t.boolean "country_default", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "courses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "course_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "disease_members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id", null: false
    t.uuid "disease_id", null: false
    t.boolean "disease_m_status", default: false
    t.string "disease_m_description"
    t.datetime "disease_m_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "diseases", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "diseases_name"
    t.boolean "diseases_status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "districts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "region_id"
    t.string "district_name"
    t.integer "district_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "group_units", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "unit_id"
    t.uuid "group_id"
    t.integer "group_unit_status", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "district_id"
    t.string "group_name"
    t.integer "group_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "handicap_members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id", null: false
    t.uuid "handicap_id", null: false
    t.boolean "handicap_m_status", default: false
    t.string "handicap_m_description"
    t.datetime "handicap_m_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "handicaps", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "handicaps_name"
    t.boolean "handicaps_status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "histories", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "histories_name"
    t.boolean "histories_status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "history_members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id", null: false
    t.uuid "history_id", null: false
    t.boolean "history_m_status", default: false
    t.string "history_m_description"
    t.datetime "history_m_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "intolerance_members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id", null: false
    t.uuid "intolerance_id", null: false
    t.boolean "intolerance_m_status", default: false
    t.string "intolerance_m_description"
    t.datetime "intolerance_m_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "intolerances", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "intolerances_name"
    t.boolean "intolerances_status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "jobtitle_members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "jobtitle_id"
    t.uuid "member_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "jobtitles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "jobtitle_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "legal_tutors", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id"
    t.uuid "state_id"
    t.uuid "province_id"
    t.uuid "parish_id"
    t.uuid "religion_id"
    t.string "cedula", limit: 15
    t.string "gender"
    t.string "first_name"
    t.string "middle_name"
    t.string "last_name"
    t.string "second_surname"
    t.string "address"
    t.string "ocupation"
    t.string "ocupation_place"
    t.integer "ocupation_type", default: 0
    t.string "phone"
    t.integer "level_education", default: 0
    t.string "age"
    t.datetime "birthdate", null: false
    t.string "second_email"
    t.string "citizenship", default: "0"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "limit_ages", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "unit_id"
    t.integer "units_limit_start"
    t.integer "units_limit_end"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_files", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id", null: false
    t.string "blood_type"
    t.string "height"
    t.string "weight"
    t.string "insurance_name"
    t.string "insurance_number"
    t.string "emergency_first_name"
    t.string "emergency_last_name"
    t.string "emergency_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "member_advances", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id"
    t.uuid "advance_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "member_trainings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id"
    t.uuid "training_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "group_unit_id"
    t.uuid "state_id"
    t.uuid "province_id"
    t.uuid "parish_id"
    t.uuid "religion_id"
    t.string "cedula", limit: 15
    t.string "gender"
    t.string "first_name"
    t.string "middle_name"
    t.string "last_name"
    t.string "second_surname"
    t.string "address"
    t.string "ocupation"
    t.string "ocupation_place"
    t.integer "ocupation_type", default: 0
    t.string "phone"
    t.integer "level_education", default: 0
    t.integer "age"
    t.datetime "birthdate", null: false
    t.datetime "date_admission"
    t.datetime "date_promise"
    t.integer "member_type", default: 0
    t.integer "is_adult", default: 0
    t.string "second_email"
    t.integer "status_renewal", default: 0, null: false
    t.integer "citizenship", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "opportunities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "opportunities_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "parishes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "province_id"
    t.string "parishe_name"
    t.boolean "archived", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "id,province_id,parishe_name,archived,created_at,updated_at", limit: 256
  end

  create_table "payments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "user_id"
    t.decimal "payment_mount", precision: 10, scale: 2
    t.date "payment_date"
    t.date "renewal_start"
    t.date "renewal_end"
    t.integer "payment_status"
    t.string "payment_reference"
    t.string "file"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "permissions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.integer "permission_type", null: false
    t.string "permission_name", null: false
    t.string "description", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "provinces", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "state_id"
    t.string "province_name"
    t.boolean "archived", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "regions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "country_id"
    t.string "region_name"
    t.integer "region_status"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "religions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "religion_name"
    t.boolean "archived", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "role_permissions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "role_id"
    t.uuid "permission_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "rol_name", null: false
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "states", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "country_id"
    t.string "state_name"
    t.boolean "archived", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "training_opportunities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "training_id"
    t.uuid "opportunity_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "trainings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "training_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "units", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "unit_name"
    t.string "unit_gender"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.uuid "member_id"
    t.uuid "role_id"
    t.integer "completeness", default: 0, null: false
    t.integer "status", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "vaccine_members", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "member_id", null: false
    t.uuid "vaccine_id", null: false
    t.boolean "vaccine_m_status", default: false
    t.string "vaccine_m_description"
    t.datetime "vaccine_m_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "vaccines", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "vaccine_name"
    t.boolean "vaccine_status", default: true
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "advance_courses", "advances"
  add_foreign_key "advance_courses", "courses"
  add_foreign_key "advances", "units"
  add_foreign_key "allergy_members", "allergies"
  add_foreign_key "allergy_members", "members"
  add_foreign_key "corporate_members", "corporate_jobtitles"
  add_foreign_key "corporate_members", "districts"
  add_foreign_key "corporate_members", "members"
  add_foreign_key "corporate_members", "regions"
  add_foreign_key "disease_members", "diseases"
  add_foreign_key "disease_members", "members"
  add_foreign_key "districts", "regions"
  add_foreign_key "group_units", "groups"
  add_foreign_key "group_units", "units"
  add_foreign_key "groups", "districts"
  add_foreign_key "handicap_members", "handicaps"
  add_foreign_key "handicap_members", "members"
  add_foreign_key "history_members", "histories"
  add_foreign_key "history_members", "members"
  add_foreign_key "intolerance_members", "intolerances"
  add_foreign_key "intolerance_members", "members"
  add_foreign_key "jobtitle_members", "jobtitles"
  add_foreign_key "jobtitle_members", "members"
  add_foreign_key "legal_tutors", "members"
  add_foreign_key "limit_ages", "units"
  add_foreign_key "medical_files", "members"
  add_foreign_key "member_advances", "advances"
  add_foreign_key "member_advances", "members"
  add_foreign_key "member_trainings", "members"
  add_foreign_key "member_trainings", "trainings"
  add_foreign_key "parishes", "provinces"
  add_foreign_key "payments", "users"
  add_foreign_key "provinces", "states"
  add_foreign_key "regions", "countries"
  add_foreign_key "role_permissions", "permissions"
  add_foreign_key "role_permissions", "roles"
  add_foreign_key "states", "countries"
  add_foreign_key "training_opportunities", "opportunities"
  add_foreign_key "training_opportunities", "trainings"
  add_foreign_key "users", "members"
  add_foreign_key "users", "roles"
  add_foreign_key "vaccine_members", "members"
  add_foreign_key "vaccine_members", "vaccines"
end
