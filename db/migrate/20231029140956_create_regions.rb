class CreateRegions < ActiveRecord::Migration[7.0]
  def change
    create_table :regions, id: :uuid do |t|
      t.uuid :country_id
      t.string :region_name
      t.integer :region_status

      t.timestamps
    end

    add_foreign_key :regions, :countries, column: :country_id
  end
end