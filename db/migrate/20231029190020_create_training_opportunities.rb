class CreateTrainingOpportunities < ActiveRecord::Migration[7.0]
  def change
    create_table :training_opportunities, id: :uuid do |t|
      t.uuid :training_id
      t.uuid :opportunity_id

      t.timestamps
    end

    add_foreign_key :training_opportunities, :trainings, column: :training_id
    add_foreign_key :training_opportunities, :opportunities, column: :opportunity_id
  end
end
