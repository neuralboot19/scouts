class CreateStates < ActiveRecord::Migration[7.0]
  def change
    create_table :states, id: :uuid do |t|
      t.uuid :country_id
      t.string :state_name
      t.boolean :archived, default: true

      t.timestamps
    end

    add_foreign_key :states, :countries, column: :country_id
  end
end
