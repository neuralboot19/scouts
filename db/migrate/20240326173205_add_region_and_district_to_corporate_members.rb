class AddRegionAndDistrictToCorporateMembers < ActiveRecord::Migration[7.0]
  def change
    add_reference :corporate_members, :region, type: :uuid, foreign_key: true
    add_reference :corporate_members, :district, type: :uuid, foreign_key: true
  end
end
