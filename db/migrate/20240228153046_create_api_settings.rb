class CreateApiSettings < ActiveRecord::Migration[7.0]
  def change
    create_table :api_settings do |t|
      t.string :api_url
      t.string :api_token
      t.string :api_token_refresh
      t.string :api_password
      t.string :api_dni
      t.string :expire_date

      t.timestamps
    end
  end
end
