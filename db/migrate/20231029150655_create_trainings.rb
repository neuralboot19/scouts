class CreateTrainings < ActiveRecord::Migration[7.0]
  def change
    create_table :trainings, id: :uuid do |t|
      t.string :training_name

      t.timestamps
    end
  end
end
