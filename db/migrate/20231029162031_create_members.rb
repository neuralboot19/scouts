class CreateMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :members, id: :uuid do |t|
      t.uuid :group_unit_id
      t.uuid :state_id
      t.uuid :province_id
      t.uuid :parish_id
      t.uuid :religion_id
      t.string :cedula, limit: 15
      t.string :gender
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :second_surname
      t.string :address
      t.string :ocupation
      t.string :ocupation_place
      t.integer :ocupation_type, default: 0
      t.string :phone
      t.integer :level_education, default: 0
      t.integer :age
      t.datetime :birthdate, null: false
      t.datetime :date_admission
      t.datetime :date_promise
      t.integer :member_type, default: 0
      t.integer :is_adult, default: 0
      t.string :second_email
      t.integer :status_renewal, null: false, default: 0
      t.integer :citizenship, default: 0

      t.timestamps
    end
  end
end
