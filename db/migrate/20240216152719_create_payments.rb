class CreatePayments < ActiveRecord::Migration[7.0]
  def change
    create_table :payments, id: :uuid do |t|
      t.uuid :user_id
      t.decimal :payment_mount, precision: 10, scale: 2
      t.date :payment_date
      t.date :renewal_start
      t.date :renewal_end
      t.integer :payment_status
      t.string :payment_reference
      t.string :file

      t.timestamps
    end

    add_foreign_key :payments, :users, column: :user_id
  end
end
