class CreateIntoleranceMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :intolerance_members, id: :uuid do |t|
      t.uuid :member_id, null: false, foreign_key: true
      t.uuid :intolerance_id, null: false, foreign_key: true
      t.boolean :intolerance_m_status, default: false
      t.string :intolerance_m_description
      t.datetime :intolerance_m_date

      t.timestamps
    end

    add_foreign_key :intolerance_members, :members, column: :member_id
    add_foreign_key :intolerance_members, :intolerances, column: :intolerance_id
  end
end
