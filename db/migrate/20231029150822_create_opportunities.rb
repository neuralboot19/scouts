class CreateOpportunities < ActiveRecord::Migration[7.0]
  def change
    create_table :opportunities, id: :uuid do |t|
      t.string :opportunities_name

      t.timestamps
    end
  end
end
