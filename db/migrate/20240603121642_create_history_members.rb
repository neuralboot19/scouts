class CreateHistoryMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :history_members, id: :uuid do |t|
      t.uuid :member_id, null: false, foreign_key: true
      t.uuid :history_id, null: false, foreign_key: true
      t.boolean :history_m_status, default: false
      t.string :history_m_description
      t.datetime :history_m_date

      t.timestamps
    end

    add_foreign_key :history_members, :members, column: :member_id
    add_foreign_key :history_members, :histories, column: :history_id
  end
end
