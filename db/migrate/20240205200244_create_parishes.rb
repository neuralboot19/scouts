class CreateParishes < ActiveRecord::Migration[7.0]
  def change
    create_table :parishes, id: :uuid do |t|
      t.uuid :province_id
      t.string :parishe_name
      t.boolean :archived, default: true

      t.timestamps
    end

    add_foreign_key :parishes, :provinces, column: :province_id
  end
end
