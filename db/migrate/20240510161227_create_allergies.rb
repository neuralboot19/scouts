class CreateAllergies < ActiveRecord::Migration[7.0]
  def change
    create_table :allergies, id: :uuid do |t|
      t.string :allergies_name
      t.boolean :allergies_status, default: true

      t.timestamps
    end
  end
end
