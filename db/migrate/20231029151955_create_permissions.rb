class CreatePermissions < ActiveRecord::Migration[7.0]
  def change
    create_table :permissions, id: :uuid do |t|
      t.integer :permission_type, null: false
      t.string :permission_name, null: false
      t.string :description, null: false

      t.timestamps
    end
  end
end
