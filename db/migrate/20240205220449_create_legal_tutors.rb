class CreateLegalTutors < ActiveRecord::Migration[7.0]
  def change
    create_table :legal_tutors, id: :uuid do |t|
      t.uuid :member_id
      t.uuid :state_id
      t.uuid :province_id
      t.uuid :parish_id
      t.uuid :religion_id
      t.string :cedula, limit: 15
      t.string :gender
      t.string :first_name
      t.string :middle_name
      t.string :last_name
      t.string :second_surname
      t.string :address
      t.string :ocupation
      t.string :ocupation_place
      t.integer :ocupation_type, default: 0
      t.string :phone
      t.integer :level_education, default: 0
      t.string :age
      t.datetime :birthdate, null: false
      t.string :second_email
      t.string :citizenship, default: 0

      t.timestamps
    end

    add_foreign_key :legal_tutors, :members, column: :member_id
  end
end
