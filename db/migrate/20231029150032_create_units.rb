class CreateUnits < ActiveRecord::Migration[7.0]
  def change
    create_table :units, id: :uuid do |t|
      t.string :unit_name
      t.string :unit_gender

      t.timestamps
    end
  end
end
