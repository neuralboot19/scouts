class CreateReligions < ActiveRecord::Migration[7.0]
  def change
    create_table :religions, id: :uuid do |t|
      t.string :religion_name
      t.boolean :archived, default: true

      t.timestamps
    end
  end
end
