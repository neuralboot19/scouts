class CreateIntolerances < ActiveRecord::Migration[7.0]
  def change
    create_table :intolerances, id: :uuid do |t|
      t.string :intolerances_name
      t.boolean :intolerances_status, default: true

      t.timestamps
    end
  end
end
