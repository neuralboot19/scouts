class CreateDiseaseMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :disease_members, id: :uuid do |t|
      t.uuid :member_id, null: false, foreign_key: true
      t.uuid :disease_id, null: false, foreign_key: true
      t.boolean :disease_m_status, default: false
      t.string :disease_m_description
      t.datetime :disease_m_date

      t.timestamps
    end

    add_foreign_key :disease_members, :members, column: :member_id
    add_foreign_key :disease_members, :diseases, column: :disease_id
  end
end
