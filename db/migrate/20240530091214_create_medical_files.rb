class CreateMedicalFiles < ActiveRecord::Migration[7.0]
  def change
    create_table :medical_files, id: :uuid do |t|
      t.uuid :member_id, null: false, foreign_key: true
      t.string :blood_type
      t.string :height
      t.string :weight
      t.string :insurance_name
      t.string :insurance_number
      t.string :emergency_first_name
      t.string :emergency_last_name
      t.string :emergency_number

      t.timestamps
    end

    add_foreign_key :medical_files, :members, column: :member_id
  end
end
