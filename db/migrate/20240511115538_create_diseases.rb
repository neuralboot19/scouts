class CreateDiseases < ActiveRecord::Migration[7.0]
  def change
    create_table :diseases, id: :uuid do |t|
      t.string :diseases_name
      t.boolean :diseases_status, default: true

      t.timestamps
    end
  end
end
