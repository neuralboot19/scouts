class CreateVaccines < ActiveRecord::Migration[7.0]
  def change
    create_table :vaccines, id: :uuid do |t|
      t.string :vaccine_name
      t.boolean :vaccine_status, default: true

      t.timestamps
    end
  end
end
