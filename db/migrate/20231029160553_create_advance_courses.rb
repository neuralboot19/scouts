class CreateAdvanceCourses < ActiveRecord::Migration[7.0]
  def change
    create_table :advance_courses, id: :uuid do |t|
      t.uuid :advance_id
      t.uuid :course_id
      t.timestamps
    end

    add_foreign_key :advance_courses, :advances, column: :advance_id
    add_foreign_key :advance_courses, :courses, column: :course_id
  end
end
