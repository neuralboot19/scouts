class CreateHistories < ActiveRecord::Migration[7.0]
  def change
    create_table :histories, id: :uuid do |t|
      t.string :histories_name
      t.boolean :histories_status, default: true

      t.timestamps
    end
  end
end
