class CreateDistricts < ActiveRecord::Migration[7.0]
  def change
    create_table :districts, id: :uuid do |t|
      t.uuid :region_id
      t.string :district_name
      t.integer :district_status

      t.timestamps
    end

    add_foreign_key :districts, :regions, column: :region_id
  end
end
