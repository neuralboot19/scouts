class CreateCorporateMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :corporate_members, id: :uuid do |t|
      t.uuid :member_id
      t.uuid :corporate_jobtitle_id

      t.timestamps
    end

    add_foreign_key :corporate_members, :members, column: :member_id
    add_foreign_key :corporate_members, :corporate_jobtitles, column: :corporate_jobtitle_id
  end
end
