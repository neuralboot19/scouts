class CreateGroups < ActiveRecord::Migration[7.0]
  def change
    create_table :groups, id: :uuid do |t|
      t.uuid :district_id
      t.string :group_name
      t.integer :group_status

      t.timestamps
    end

    add_foreign_key :groups, :districts, column: :district_id
  end
end
