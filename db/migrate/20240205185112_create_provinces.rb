class CreateProvinces < ActiveRecord::Migration[7.0]
  def change
    create_table :provinces, id: :uuid do |t|
      t.uuid :state_id
      t.string :province_name
      t.boolean :archived, default: true

      t.timestamps
    end

    add_foreign_key :provinces, :states, column: :state_id
  end
end
