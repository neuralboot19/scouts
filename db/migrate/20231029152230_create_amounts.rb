class CreateAmounts < ActiveRecord::Migration[7.0]
  def change
    create_table :amounts, id: :uuid do |t|
      t.string :amount_name
      t.decimal :amount_usd, precision: 10, scale: 2
      t.decimal :amount_exchange, precision: 10, scale: 2
      t.boolean :amount_status
      t.integer :amount_type, null: false, default: 0

      t.timestamps
    end
  end
end
