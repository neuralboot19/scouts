class CreateGroupUnits < ActiveRecord::Migration[7.0]
  def change
    create_table :group_units, id: :uuid do |t|
      t.uuid :unit_id
      t.uuid :group_id
      t.integer :group_unit_status, default: 0

      t.timestamps
    end

    add_foreign_key :group_units, :units, column: :unit_id
    add_foreign_key :group_units, :groups, column: :group_id
  end
end
