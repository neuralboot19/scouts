class CreateMemberAdvances < ActiveRecord::Migration[7.0]
  def change
    create_table :member_advances, id: :uuid do |t|
      t.uuid :member_id
      t.uuid :advance_id

      t.timestamps
    end

    add_foreign_key :member_advances, :members, column: :member_id
    add_foreign_key :member_advances, :advances, column: :advance_id
  end
end
