class CreateCountries < ActiveRecord::Migration[7.0]
  def change
    create_table :countries, id: :uuid do |t|
      t.string :country_name
      t.integer :country_type, default: 0
      t.boolean :archived, default: false
      t.boolean :country_default, default: false

      t.timestamps
    end
  end
end
