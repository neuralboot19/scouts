class CreateMemberTrainings < ActiveRecord::Migration[7.0]
  def change
    create_table :member_trainings, id: :uuid do |t|
      t.uuid :member_id
      t.uuid :training_id

      t.timestamps
    end

    add_foreign_key :member_trainings, :members, column: :member_id
    add_foreign_key :member_trainings, :trainings, column: :training_id
  end
end
