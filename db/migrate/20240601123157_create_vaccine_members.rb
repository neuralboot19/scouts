class CreateVaccineMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :vaccine_members, id: :uuid do |t|
      t.uuid :member_id, null: false, foreign_key: true
      t.uuid :vaccine_id, null: false, foreign_key: true
      t.boolean :vaccine_m_status, default: false
      t.string :vaccine_m_description
      t.datetime :vaccine_m_date

      t.timestamps
    end

    add_foreign_key :vaccine_members, :members, column: :member_id
    add_foreign_key :vaccine_members, :vaccines, column: :vaccine_id
  end
end
