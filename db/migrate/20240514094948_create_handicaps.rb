class CreateHandicaps < ActiveRecord::Migration[7.0]
  def change
    create_table :handicaps, id: :uuid do |t|
      t.string :handicaps_name
      t.boolean :handicaps_status, default: true

      t.timestamps
    end
  end
end
