class CreateHandicapMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :handicap_members, id: :uuid do |t|
      t.uuid :member_id, null: false, foreign_key: true
      t.uuid :handicap_id, null: false, foreign_key: true
      t.boolean :handicap_m_status, default: false
      t.string :handicap_m_description
      t.datetime :handicap_m_date

      t.timestamps
    end

    add_foreign_key :handicap_members, :members, column: :member_id
    add_foreign_key :handicap_members, :handicaps, column: :handicap_id
  end
end
