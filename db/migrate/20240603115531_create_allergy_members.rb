class CreateAllergyMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :allergy_members, id: :uuid do |t|
      t.uuid :member_id, null: false, foreign_key: true
      t.uuid :allergy_id, null: false, foreign_key: true
      t.boolean :allergy_m_status, default: false
      t.string :allergy_m_description
      t.datetime :allergy_m_date

      t.timestamps
    end

    add_foreign_key :allergy_members, :members, column: :member_id
    add_foreign_key :allergy_members, :allergies, column: :allergy_id
  end
end
