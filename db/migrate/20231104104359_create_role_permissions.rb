class CreateRolePermissions < ActiveRecord::Migration[7.0]
  def change
    create_table :role_permissions, id: :uuid do |t|
      t.uuid :role_id
      t.uuid :permission_id

      t.timestamps
    end

    add_foreign_key :role_permissions, :roles, column: :role_id
    add_foreign_key :role_permissions, :permissions, column: :permission_id
  end
end
