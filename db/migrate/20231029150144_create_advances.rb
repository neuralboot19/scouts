class CreateAdvances < ActiveRecord::Migration[7.0]
  def change
    create_table :advances, id: :uuid do |t|
      t.uuid :unit_id
      t.string :advance_name
      t.integer :secuencial

      t.timestamps
    end

    add_foreign_key :advances, :units, column: :unit_id
  end
end
