class CreateJobtitles < ActiveRecord::Migration[7.0]
  def change
    create_table :jobtitles, id: :uuid do |t|
      t.string :jobtitle_name

      t.timestamps
    end
  end
end
