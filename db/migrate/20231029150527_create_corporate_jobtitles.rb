class CreateCorporateJobtitles < ActiveRecord::Migration[7.0]
  def change
    create_table :corporate_jobtitles, id: :uuid do |t|
      t.string :corporate_jobtitle_name

      t.timestamps
    end
  end
end
