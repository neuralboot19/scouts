class CreateLimitAges < ActiveRecord::Migration[7.0]
  def change
    create_table :limit_ages, id: :uuid do |t|
      t.uuid :unit_id
      t.integer :units_limit_start
      t.integer :units_limit_end

      t.timestamps
    end

    add_foreign_key :limit_ages, :units, column: :unit_id
  end
end
