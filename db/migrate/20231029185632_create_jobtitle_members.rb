class CreateJobtitleMembers < ActiveRecord::Migration[7.0]
  def change
    create_table :jobtitle_members, id: :uuid do |t|
      t.uuid :jobtitle_id
      t.uuid :member_id

      t.timestamps
    end

    add_foreign_key :jobtitle_members, :jobtitles, column: :jobtitle_id
    add_foreign_key :jobtitle_members, :members, column: :member_id
  end
end
