class CreateRoles < ActiveRecord::Migration[7.0]
  def change
    create_table :roles, id: :uuid do |t|
      t.string :rol_name, null: false
      t.string :description

      t.timestamps
    end
  end
end
