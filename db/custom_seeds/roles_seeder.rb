class RolesSeeder
  def self.seed
    admin = create_role('Admin', 'Superadministrador')
    user = create_role('User', 'Users')
    [admin, user]
  end

  def self.create_role(rol_name, description)
    Role.find_or_create_by rol_name:, description:
  end
end
