class UsersSeeder
  def self.seed(email, password, role, member = nil)
    User.find_or_create_by(email:) do |user|
      user.email = email
      user.password = password
      user.password_confirmation = password
      user.role_id = role.id
      user.member_id = member&.id
    end
  end
end
