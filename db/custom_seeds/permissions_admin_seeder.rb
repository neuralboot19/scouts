class PermissionsAdminSeeder
  def self.seed
    Permission.find_or_create_by permission_type: :superadmin,
                                 permission_name: 'Superadmin',
                                 description: 'Todos los permisos'
  end
end
