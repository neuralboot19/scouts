# frozen_string_literal: true

# lib/tasks/enabled_user_whith_payment.rake

namespace :enabled_users do
  desc 'Enabled user whith payment'
  task enabled_users: :environment do
    # Realiza la consulta para obtener los pagos vencidos con estado de pago 1
    ps = Payment.where('renewal_end > ? AND payment_status = ?', Date.today, 1)

    # Obtén los usuarios asociados a los pagos vencidos
    usuarios_pagos_vencidos = ps.map(&:user)

    # Cambia el payment_status a 0 en los pagos resultantes de la consulta ps
    ps.update_all(payment_status: 1, updated_at: Date.today)

    # Actualización de usuario sobre el pago activo
    usuarios_pagos_vencidos.each do |usuario|
      usuario.status = 1
      if usuario.member.present?
        usuario.member.update(status_renewal: 0)
      else
        puts "ERROR user sin member =>>>>>>>>>>>>>>>>>>>>> #{usuario.email}"
      end
      usuario.save
    end
  end
end
