# frozen_string_literal: true

# lib/tasks/expiration_payment.rake

namespace :expiration_payment do
  desc 'Expiration payment'
  task expiration_payment: :environment do
    one_month_ago = Date.today - 1.month

    active_payments = Payment.where(
      renewal_end: one_month_ago..Date.today,
      payment_status: 1
    )

    active_payments.each do |payment|
      PaymentMailer.expiration_payment_email(payment.user).deliver_now
    end
  end
end
