# frozen_string_literal: true

# lib/tasks/disabled_user_whith_payment.rake

namespace :disabled_users do
  desc 'Disabled user whith payment'
  task disabled_users: :environment do
    # Realiza la consulta para obtener los pagos vencidos con estado de pago 1
    ps = Payment.where('renewal_end < ? AND payment_status = ?', Date.today, 1)

    # Obtén los usuarios asociados a los pagos vencidos
    usuarios_pagos_vencidos = ps.map(&:user)

    # Cambia el payment_status a 0 en los pagos resultantes de la consulta ps
    ps.update_all(payment_status: 2, updated_at: Date.today)

    # Actualización de usuario sobre el pago vencido
    usuarios_pagos_vencidos.each do |usuario|
      usuario.status = 0
      usuario.member.update(status_renewal: 1)
      usuario.save
    end
  end
end
