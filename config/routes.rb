Rails.application.routes.draw do
  namespace :api, defaults: { format: :json } do
    namespace :v1 do
      resources :home do
        collection do
          post :search
        end
      end
    end
  end

  devise_for :users, controllers: {
    omniauth_callbacks: 'users/omniauth_callbacks',
    sessions: 'users/sessions',
    registrations: 'users/registrations',
    confirmations: 'users/confirmations',
    passwords: 'users/passwords'
  }

  devise_scope :user do
    authenticated :user do
      root to: 'pages#index', as: :authenticated_root
      get '/users/sign_out', to: 'devise/sessions#destroy'

      resources :members, only: %i[index new create edit update] do
        collection do
          get :regions
          get :districts
          get :groups
          get :units
          get :advance
          get :states
          get :provinces
          post :verificate
        end
      end

      get '/members/grupo'
      get '/members/group_youth'
      get '/members/dualidad'
      get '/members/institucional'
      get '/members/legal_tutor'
      post '/members/grupo_submit'
      post '/members/group_youth_submit'
      post '/members/dualidad_submit'
      post '/members/institucional_submit'
      post '/members/legal_tutor_submit'
      post '/members/verificate'

      namespace :users do
        resources :amounts
        resources :profiles, only: %i[index]
        resources :resets, only: %i[update] do
          collection do
            get :group
            get :district
            get :region
          end
        end
        resources :reports do
          collection do
            get :payments
            get :members
            get :tutors_legals
            get :numerical_cutoff
            get :my_group
            get :my_district
            get :my_region
            get :reports_by_payments, format: :xlsx
            get :reports_by_members, format: :xlsx
            get :reports_by_tutors_legals, format: :xlsx
            get :reports_by_numerical_cutoff, format: :xlsx
            get :reports_by_my_group, format: :xlsx
            get :reports_by_my_district, format: :xlsx
            get :reports_by_my_region, format: :xlsx
          end
        end
        resources :payments, only: %i[index new create show destroy] do
          collection do
            get :checkings
            post :swap
          end
        end
        resources :roles, except: [:show]
        resources :vaccines, except: [:show]
        resources :allergies, excep: [:show]
        resources :diseases, excep: [:show]
        resources :histories, excep: [:show]
        resources :handicaps, excep: [:show]
        resources :intolerances, excep: [:show]
        resources :medical_files, only: %i[new edit create update]
        resources :assignments, only: %i[index] do
          collection do
            post :assignment
          end
        end
      end
    end

    unauthenticated :user do
      root to: 'users/sessions#new', as: :unauthenticated_root
    end

    get 'static_pages/no_permissions', to: 'static_pages#no_permissions', as: 'static_pages_no_permissions'
  end
end
