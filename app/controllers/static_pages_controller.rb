class StaticPagesController < ApplicationController
  skip_after_action :verify_authorized

  def no_permissions
    @referer = request.referer
  end
end
