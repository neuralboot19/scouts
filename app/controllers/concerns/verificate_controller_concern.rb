module VerificateControllerConcern
  extend ActiveSupport::Concern
  # divide y venceras
  def verificate(resource)
    @member = resource.member
    session[:member] = @member.id unless @member.nil?
    redirection(@member, resource)
  end

  def validate_payment
    if user_signed_in?
      redirect_to users_payments_path if !current_user.status_payments?
    end
  end

  def set_date_pay
    if user_signed_in?
      if current_user.status_payments?
        p = Payment.where(user: current_user, payment_status:'activo').first
        date_validate = p.renewal_end - 1.month
        @active_pay_date = Date.today >= date_validate
      end
    end
  end

  private

  def redirection(member, resource)
    if member.nil?
      members_path
    elsif !member.cedula.present? || member&.cedula == ''
      members_path
    elsif resource.completeness < 100
      edit_member_path(member)
    elsif !resource.status_payments?
      users_payments_path
    elsif !member.group_unit.present? && !member.member_type == "institucional"
      members_grupo_path
    elsif (member.group_unit.present? || member.member_type == "institucional") && member.cedula.present?
      authenticated_root_path
    end
  end
end
