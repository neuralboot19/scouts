class PagesController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment

  def index
    authorize %i[pages]
  end
end
