class Api::V1::HomeController < ApplicationController
  before_action :authenticate_user!, except: [:search]
  skip_after_action :verify_authorized, only: [:search]
  skip_before_action :verify_authenticity_token, only: [:search]

  def search
    email = params[:email]
    user = User.find_by(email: email)

    if user
      render json: { status: user.status, cedula: user&.member&.cedula, email: user.email }
    else
      render json: { error: 'Usuario no encontrado' }, status: :not_found
    end
  end
end
