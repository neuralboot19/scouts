class ApplicationController < ActionController::Base
  include Pundit::Authorization
  include VerificateControllerConcern

  before_action :authenticate_user!
  after_action :verify_authorized, unless: :devise_controller?
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized
  before_action :set_date_pay

  private

  def user_not_authorized
    redirect_to static_pages_no_permissions_path
  end
end
