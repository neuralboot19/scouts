class Users::ResetsController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment

  def group
    authorize [:users, :resets]

    # Encuentra el miembro del current_user
    member = current_user.member

    # Encuentra el grupo del miembro
    group = member.group_unit.group

    # Encuentra todas las unidades asociadas al grupo
    unidades_del_grupo = Unit.joins(:group_units).where(group_units: { group_id: group.id })

    @users_resets = []
    unidades_del_grupo.each do |unidad|
      miembros_unidad = Member.joins(group_unit: :group).where(group_units: { unit_id: unidad.id, group_id: group.id })

      # Inicializar la busqueda de miembros con estatus activos del modelo user
      miembros_unidad.each do |member|
        u = User.where(member: member, status: 1)
        @users_resets << u if u.present?
      end
    end
  end

  def district
    authorize [:users, :resets]

    # Encuentra el miembro corporate del current_user
    corporate = CorporateMember.find_by_member_id(current_user.member.id)

    # Encuentra el distrito del miembro corporate
    district = corporate.district

    # Encuentra todos los grupos asociados al distrito
    groups_in_district = Group.where(district_id: district.id)

    # Inicializa un arreglo para almacenar todos los miembros activos
    @users_resets = []

    # Itera sobre cada grupo del distrito
    groups_in_district.each do |group|

      # Encuentra todas las unidades asociadas al grupo
      unidades_del_grupo = Unit.joins(:group_units).where(group_units: { group_id: group.id })

      # Itera sobre cada unidad del grupo
      unidades_del_grupo.each do |unidad|
        # Encuentra todos los miembros de la unidad actual
        miembros_unidad = Member.joins(group_unit: :group).where(group_units: { unit_id: unidad.id, group_id: group.id })

        # Inicializar la busqueda de miembros con estatus activos del modelo user
        miembros_unidad.each do |member|
          u = User.where(member: member, status: 1)
          @users_resets << [u, group] if u.present?
        end
      end
    end
  end

  def region
    authorize [:users, :resets]

    # Encuentra el miembro corporate del current_user
    corporate = CorporateMember.find_by_member_id(current_user.member.id)

    # Encuentra la región del miembro corporate
    region = corporate.region

    # Encuentra los distritos de la región
    districts_in_region = District.where(region_id: region.id)

    # Inicializa un arreglo para almacenar todos los miembros activos por región
    @users_resets = []

    # Itera sobre cada distrito de la región
    districts_in_region.each do |district|
      # Encuentra todos los grupos asociados al distrito
      groups_in_district = Group.where(district_id: district.id)

      # Itera sobre cada grupo del distrito
      groups_in_district.each do |group|
        # Encuentra todas las unidades asociadas al grupo
        unidades_del_grupo = Unit.joins(:group_units).where(group_units: { group_id: group.id })

        # Itera sobre cada unidad del grupo
        unidades_del_grupo.each do |unidad|
          # Encuentra todos los miembros de la unidad actual
          miembros_unidad = Member.joins(group_unit: :group).where(group_units: { unit_id: unidad.id, group_id: group.id })

          # Inicializar la búsqueda de miembros con estatus activos del modelo user
          miembros_unidad.each do |member|
            u = User.where(member: member, status: 1)
            @users_resets << [u, group, district] if u.present?
          end
        end
      end
    end
  end

  # PATCH/PUT /users/resets/1 or /users/resets/1.json
  def update
    authorize [:users, :resets]

    user_reset = User.find_by_id(params[:id])

    jm = JobtitleMember.find_by_member_id(user_reset.member.id)
    jm.delete if jm.present?

    cm = CorporateMember.find_by_member_id(user_reset.member.id)
    cm.delete if cm.present?

    gu_id = user_reset.member.group_unit_id

    member = Member.find_by_id(user_reset.member.id)
    member.second_email = nil
    user_reset.update(completeness: member.member_completeness)

    if gu_id.present?
      member.update(group_unit_id: nil)
    end

    redirect_to authenticated_root_path, notice: 'Reiniciado con éxito'
  end
end
