class Users::PaymentsController < ApplicationController
  before_action :set_payment, only: %i[show]
  before_action :set_pay_validated, only: %i[create index show]

  def index
    authorize %i[users payments]
    return redirect_to static_pages_no_permissions_path if @active_pay_date == false
    @amounts = Amount.where(amount_status: true)
  end

  def new
    authorize %i[users payments]
  end

  def create
    authorize %i[users payments]
    amount = session[:amount]
    amount_total = amount['amount_usd'].to_f * amount['amount_exchange'].to_f
    amount_total = (amount['amount_usd'].to_f - 2) * amount['amount_exchange'].to_f if @validate_discount
    payment_enabled = Payment.where(user: current_user, payment_status: 1).last
    exists_reference = Payment.find_by_payment_reference(payment_params[:number_reference])
    if exists_reference.present?
      redirect_to users_payments_path, notice: 'Ya fue usado. Realize su pago.'
    else
      payment = Payment.new(
        user: current_user,
        payment_date: Date.today,
        renewal_start: Date.today,
        renewal_end: Date.today + 1.year,
        payment_reference: payment_params[:number_reference],
        payment_mount: amount_total
      )

      case amount['amount_type']
      when 'movil'
        handle_movil_payment(payment, payment_enabled)
      when 'manual'
        handle_manual_payment(payment)
      end
    end
  end

  def destroy
    authorize %i[users payments]

    payment = Payment.find(params[:id])
    payment.user.update(status: 0) unless payment.user.status == "activo"
    PaymentMailer.declined_email(payment.user).deliver_now
    payment.destroy

    redirect_to checkings_users_payments_path, notice: 'Pago declinado por usted.' if payment.destroy
  end

  def show
    authorize %i[users payments]
    session[:amount] = @amount
    api = ApiSetting.last
    res = Bancamiga.list_banks(api)
    body_res = JSON.parse(res.body)
    @list_banks = ['Selecciona un banco']

    return unless body_res['code'] == 200

    @list_banks = body_res['data'].pluck('codBanco', 'nombreBanco').uniq if body_res['data'].present?
  end

  def checkings
    authorize %i[users payments]

    @payments = Payment.where(payment_status: 'pendiente')
  end

  def swap
    authorize %i[users payments]
    payment_search = Payment.find_by_id(swap_params[:payment])
    payment_activo = payment_search.user.payments.where(payment_status: 'activo').last

    if payment_activo.present?
      payment_activo.payment_status = 2
      payment_activo.renewal_end = Date.today
      payment_activo.save
    end

    payment_search.user.update(status: 1)
    payment_search.payment_status = 'activo'
    PaymentMailer.accepted_email(payment_search.user).deliver_now

    redirect_to checkings_users_payments_path, notice: 'Pago aprobado por usted.' if payment_search.save
  end

  private

  def set_payment
    @amount = Amount.find_by_id(params[:id])
  end

  def set_pay_validated
    @validate_discount = current_user.payment_is_validate?
  end

  def payment_params
    params.require(:payment).permit(:bank, :number_reference, :number_movil, :date_pay, :file)
  end

  def swap_params
    params.require(:payment).permit(:payment)
  end

  def handle_movil_payment(payment, payment_enabled)
    api = ApiSetting.last
    date_current = Time.now
    format_date = date_current.strftime("%Y-%m-%d")

    body = {
      Phone: payment_params[:number_movil],
      Bank: payment_params[:bank],
      Date: format_date
    }

    res = Bancamiga.find_payment(api, body.to_json)
    body_res = JSON.parse(res.body)

    return redirect_to users_payments_path, notice: 'Error Bancamiga' unless body_res['code'] == 200

    item_validated = nil

    body_res['lista'].each do |item|
      amount_pay = payment.payment_mount.to_f
      item_validated = item if item['Amount'].to_f == amount_pay && item['PhoneOrig'] == payment_params['number_movil'] && item['NroReferenciaCorto'] == payment.payment_reference && item['BancoOrig'] == payment_params['bank']
    end

    if item_validated.present?
      if payment_enabled.present?
        payment_enabled.payment_status = 2
        payment_enabled.renewal_end = Date.today
        payment_enabled.save
      end

      payment.payment_status = 'activo'
      PaymentMailer.accepted_email(payment.user).deliver_now
      return payment_find(1) if payment.save
    else
      redirect_to users_payments_path, notice: 'Compruebe sus datos'
    end
  end

  def handle_manual_payment(payment)
    payment.payment_date = payment_params[:date_pay]
    payment.renewal_start = payment_params[:date_pay]
    current_date = Date.parse(payment_params[:date_pay])
    new_date = current_date + 1.year
    payment.renewal_end = new_date.to_s
    payment.payment_status = 'pendiente'
    imagen = Cloudinary::Uploader.upload(payment_params[:file]) unless payment_params[:file].nil?
    payment.file = imagen['secure_url']
    PaymentMailer.processed_email(current_user).deliver_now
    return payment_find(0) if payment.save
  end

  def payment_find(status)
    current_user.update(status: status)
    redirect_to authenticated_root_path, notice: 'Gracias por tu pago.'
  end
end
