class Users::HistoriesController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :set_users_history, only: %i[edit update destroy]
  before_action :authorize_users_histories

  def index
    @histories = History.all
  end

  def new
    @users_history = History.new
  end

  def edit; end

  def create
    @users_history = History.new(users_history_params)

    return redirect_to users_histories_url, notice: t('.created') if @users_history.save

    render :new, status: :unprocessable_entity
  end

  def update
    return redirect_to users_histories_url, notice: t('.updated') if @users_history.update(users_history_params)

    render :edit, status: :unprocessable_entity
  end

  def destroy
    @users_history.destroy
    redirect_to users_histories_url, notice: t('.destroyed')
  end

  private

  def set_users_history
    @users_history = History.find(params[:id])
  end

  def users_history_params
    params.require(:history).permit(:histories_name, :histories_status)
  end

  def authorize_users_histories
    authorize %i[users histories]
  end
end
