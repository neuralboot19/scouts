# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]
  # before_action :set_params_user, only: [:create]

  # GET /resource/sign_up
  # def new
  #   super
  # end

  # POST /resource
  # def create
  #   # super
  #   @user = User.new(set_params_user)
  #   byebug
  #   if @user.valid?
  #     self.resource = warden.authenticate!(auth_options)
  #     if resource.confirmed?
  #       sign_in(resource_name, resource)
  #       redirect_to root_path
  #     else
  #       redirect_to new_user_confirmation_path
  #     end
  #   else
  #     super
  #   end
  # end

  # GET /resource/edit
  # def edit
  #   super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  # def destroy
  #   super
  # end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_up_params
  #   devise_parameter_sanitizer.permit(:sign_up, keys: [:attribute])
  # end

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_account_update_params
  #   devise_parameter_sanitizer.permit(:account_update, keys: [:attribute])
  # end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   # super(resource)
  #   if resource.completeness < 100
  #     edit_member_path(resource.member) if resource.member.present?
  #     members_path unless resource.member.present?
  #   else
  #     root_path
  #   end
  # end

  # The path used after sign up for inactive accounts.
  def after_inactive_sign_up_path_for(resource)
    role = Role.find_by_rol_name("User")
    resource.update(role:)
    super(resource)
  end
  # def set_params_user
  #   params.require(:user).permit(:email, :password, :password_confirmation)
  # end
end
