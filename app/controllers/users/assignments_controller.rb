class Users::AssignmentsController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :authorize_users_assignments, only: [:index, :assignment]

  def index
    @list_roles = Role.all
  end

  def assignment
    user = User.find_by_email(params[:email])
    rol = Role.find_by_id(params[:rol])

    unless user && rol
      redirect_to users_assignments_path, notice: 'No se encontró el email o el rol'
      return
    end

    user.update(role_id: rol.id)
    redirect_to users_assignments_path, notice: 'Asignado con éxito'
  end

  private

  def authorize_users_assignments
    authorize [:users, :assignments]
  end
end
