class Users::RolesController < ApplicationController
  def index
    authorize %i[users roles]

    @roles = Role.all
  end

  def new
    authorize %i[users roles]

    @role = Role.new
  end

  def edit
    authorize %i[users roles]

    get_role
  end

  def create
    authorize %i[users roles]

    @role = Role.new(role_params)

    if @role.save
      redirect_to users_roles_url, notice: t('.created')
    else
      render :new, status: :unprocessable_entity
    end
  end

  def update
    authorize %i[users roles]

    if get_role.update(role_params)
      redirect_to users_roles_url, notice: t('.updated')
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def destroy
    authorize %i[users roles]

    get_role.destroy

    respond_to do |format|
      format.html { redirect_to users_roles_url, notice: t('.destroyed') }
    end
  end

  private

  def get_role
    @role = Role.find(params[:id])
  end

  def role_params
    params.require(:role).permit(:rol_name, :description, permission_ids: [])
  end
end
