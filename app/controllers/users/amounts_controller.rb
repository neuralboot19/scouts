class Users::AmountsController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :authorize_users_amount
  before_action :set_users_amount, only: %i[ show edit update destroy ]

  def index
    @users_amounts = Amount.all
  end

  def show; end

  def new
    @users_amount = Amount.new
  end

  def edit; end

  def create
    @users_amount = Amount.new(users_amount_params)
    return redirect_to users_amount_url(@users_amount), notice: 'Monto creado con exito.' if @users_amount.save

    render :new, status: :unprocessable_entity
  end

  def update
    return redirect_to users_amount_url(@users_amount), notice: 'Monto actualizado correctamente.' if @users_amount.update(users_amount_params)

    render :edit, status: :unprocessable_entity
  end

  def destroy
    @users_amount.destroy
    redirect_to users_amounts_url, notice: 'Monto eliminado.'
  end

  private

  def set_users_amount
    @users_amount = Amount.find(params[:id])
  end

  def authorize_users_amount
    authorize [:users, :amounts]
  end

  def users_amount_params
    params.require(:amount).permit(:amount_name, :amount_usd, :amount_exchange, :amount_type, :amount_status)
  end
end
