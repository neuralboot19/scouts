class Users::MedicalFilesController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :set_users_medical_file, only: %i[edit update]
  before_action :authorize_users_medical_files

  def new
    @users_medical_file = MedicalFile.new
    load_dependencies
  end

  def edit
    load_dependencies
  end

  def create
    @users_medical_file = MedicalFile.new(users_medical_file_params.except(:medical_file))
    @users_medical_file.member = current_user.member

    if @users_medical_file.save
      update_vaccines_member_status
      update_allergies_member_status
      update_diseases_member_status
      update_histories_member_status
      update_handicaps_member_status
      update_intolerances_member_status
      redirect_to users_profiles_path, notice: t('.created')
    else
      load_dependencies
      render :new, status: :unprocessable_entity
    end
  end

  def update
    if @users_medical_file.update(users_medical_file_params.except(:medical_file))
      update_vaccines_member_status
      update_allergies_member_status
      update_diseases_member_status
      update_histories_member_status
      update_handicaps_member_status
      update_intolerances_member_status
      redirect_to users_profiles_path, notice: t('.updated')
    else
      load_dependencies
      render :edit, status: :unprocessable_entity
    end
  end

  private

  def set_users_medical_file
    @users_medical_file = MedicalFile.find(params[:id])
  end

  def users_medical_file_params
    params.require(:medical_file).permit(
      :blood_type, :height, :weight, :insurance_name, :insurance_number,
      :emergency_first_name, :emergency_last_name, :emergency_number,
      medical_file: {
        vaccine_responses: {}, allergy_responses: {}, disease_responses: {},
        history_responses: {}, handicap_responses: {}, intolerance_responses: {}
      }
    )
  end

  def authorize_users_medical_files
    authorize %i[users medical_files]
  end

  def load_dependencies
    @vaccines_member = update_vaccines_member
    @allergies_member = update_allergies_member
    @diseases_member = update_diseases_member
    @histories_member = update_histories_member
    @handicaps_member = update_handicap_member
    @intolerances_member = update_intolerance_member
  end

  def update_vaccines_member
    member_id = current_user.member.id
    res = VaccineMember.where(member_id: member_id)
    disable_inactive_vaccines(res)
    enable_new_vaccines(res)
    VaccineMember.where(member_id: member_id)
  end

  def update_allergies_member
    member_id = current_user.member.id
    res = AllergyMember.where(member_id: member_id)
    disable_inactive_allergies(res)
    enable_new_allergies(res)
    AllergyMember.where(member_id: member_id)
  end

  def update_diseases_member
    member_id = current_user.member.id
    res = DiseaseMember.where(member_id: member_id)
    disable_inactive_diseases(res)
    enable_new_diseases(res)
    DiseaseMember.where(member_id: member_id)
  end

  def update_histories_member
    member_id = current_user.member.id
    res = HistoryMember.where(member_id: member_id)
    disable_inactive_histories(res)
    enable_new_histories(res)
    HistoryMember.where(member_id: member_id)
  end

  def update_handicap_member
    member_id = current_user.member.id
    res = HandicapMember.where(member_id: member_id)
    disable_inactive_handicaps(res)
    enable_new_handicaps(res)
    HandicapMember.where(member_id: member_id)
  end

  def update_intolerance_member
    member_id = current_user.member.id
    res = IntoleranceMember.where(member_id: member_id)
    disable_inactive_intolerances(res)
    enable_new_intolerances(res)
    IntoleranceMember.where(member_id: member_id)
  end

  def disable_inactive_vaccines(vsm)
    inactive_vaccine_ids = Vaccine.inactive.pluck(:id)
    vsm.where(vaccine_id: inactive_vaccine_ids).destroy_all
  end

  def disable_inactive_allergies(asm)
    inactive_allergy_ids = Allergy.inactive.pluck(:id)
    asm.where(allergy_id: inactive_allergy_ids).destroy_all
  end

  def disable_inactive_diseases(dsm)
    inactive_disease_ids = Disease.inactive.pluck(:id)
    dsm.where(disease_id: inactive_disease_ids).destroy_all
  end

  def disable_inactive_histories(hsm)
    inactive_history_ids = History.inactive.pluck(:id)
    hsm.where(history_id: inactive_history_ids).destroy_all
  end

  def disable_inactive_handicaps(hasm)
    inactive_handicap_ids = Handicap.inactive.pluck(:id)
    hasm.where(history_id: inactive_handicap_ids).destroy_all
  end

  def disable_inactive_intolerances(ism)
    inactive_intolerance_ids = Intolerance.inactive.pluck(:id)
    ism.where(intolerance_id: inactive_intolerance_ids).destroy_all
  end

  def enable_new_vaccines(vsm)
    active_vaccine_ids = Vaccine.active.pluck(:id)
    existing_vaccine_ids = vsm.pluck(:vaccine_id)
    new_vaccine_ids = active_vaccine_ids - existing_vaccine_ids

    new_vaccine_ids.each do |vaccine_id|
      VaccineMember.create(member_id: current_user.member.id, vaccine_id: vaccine_id, vaccine_m_status: false)
    end
  end

  def enable_new_allergies(asm)
    active_allergy_ids = Allergy.active.pluck(:id)
    existing_allergy_ids = asm.pluck(:allergy_id)
    new_allergy_ids = active_allergy_ids - existing_allergy_ids

    new_allergy_ids.each do |allergy_id|
      AllergyMember.create(member_id: current_user.member.id, allergy_id: allergy_id, allergy_m_status: false)
    end
  end

  def enable_new_diseases(vsm)
    active_disease_ids = Disease.active.pluck(:id)
    existing_disease_ids = vsm.pluck(:disease_id)
    new_disease_ids = active_disease_ids - existing_disease_ids

    new_disease_ids.each do |disease_id|
      DiseaseMember.create(member_id: current_user.member.id, disease_id: disease_id, disease_m_status: false)
    end
  end

  def enable_new_histories(hsm)
    active_history_ids = History.active.pluck(:id)
    existing_history_ids = hsm.pluck(:history_id)
    new_history_ids = active_history_ids - existing_history_ids

    new_history_ids.each do |history_id|
      HistoryMember.create(member_id: current_user.member.id, history_id: history_id, history_m_status: false)
    end
  end

  def enable_new_handicaps(hasm)
    active_handicap_ids = Handicap.active.pluck(:id)
    existing_handicap_ids = hasm.pluck(:handicap_id)
    new_handicap_ids = active_handicap_ids - existing_handicap_ids

    new_handicap_ids.each do |handicap_id|
      HandicapMember.create(member_id: current_user.member.id, handicap_id: handicap_id, handicap_m_status: false)
    end
  end

  def enable_new_intolerances(ism)
    active_intolerance_ids = Intolerance.active.pluck(:id)
    existing_intolerance_ids = ism.pluck(:intolerance_id)
    new_intolerance_ids = active_intolerance_ids - existing_intolerance_ids

    new_intolerance_ids.each do |intolerance_id|
      IntoleranceMember.create(member_id: current_user.member.id, intolerance_id: intolerance_id, intolerance_m_status: false)
    end
  end

  def update_vaccines_member_status
    vaccine_responses = users_medical_file_params[:medical_file][:vaccine_responses]

    vaccine_responses.each do |vaccine_id, response|
      vaccine_member = VaccineMember.find_by(vaccine_id: vaccine_id, member_id: current_user.member.id)
      next unless vaccine_member

      vaccine_member.update(vaccine_m_status: response == "yes")
    end
  end

  def update_allergies_member_status
    allergy_responses = users_medical_file_params[:medical_file][:allergy_responses]

    allergy_responses.each do |allergy_id, response|
      allergy_member = AllergyMember.find_by(allergy_id: allergy_id, member_id: current_user.member.id)
      next unless allergy_member

      allergy_member.update(allergy_m_status: response == "yes")
    end
  end

  def update_diseases_member_status
    disease_responses = users_medical_file_params[:medical_file][:disease_responses]

    disease_responses.each do |disease_id, response|
      disease_member = DiseaseMember.find_by(disease_id: disease_id, member_id: current_user.member.id)
      next unless disease_member

      disease_member.update(disease_m_status: response == "yes")
    end
  end

  def update_histories_member_status
    history_responses = users_medical_file_params[:medical_file][:history_responses]

    history_responses.each do |history_id, response|
      history_member = HistoryMember.find_by(history_id: history_id, member_id: current_user.member.id)
      next unless history_member

      history_member.update(history_m_status: response == "yes")
    end
  end

  def update_handicaps_member_status
    handicap_responses = users_medical_file_params[:medical_file][:handicap_responses]

    handicap_responses.each do |handicap_id, response|
      handicap_member = HandicapMember.find_by(handicap_id: handicap_id, member_id: current_user.member.id)
      next unless handicap_member

      handicap_member.update(handicap_m_status: response == "yes")
    end
  end

  def update_intolerances_member_status
    intolerance_responses = users_medical_file_params[:medical_file][:intolerance_responses]

    intolerance_responses.each do |intolerance_id, response|
      intolerance_member = IntoleranceMember.find_by(intolerance_id: intolerance_id, member_id: current_user.member.id)
      next unless intolerance_member

      intolerance_member.update(intolerance_m_status: response == "yes")
    end
  end
end
