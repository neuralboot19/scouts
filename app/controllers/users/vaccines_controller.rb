class Users::VaccinesController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :set_users_vaccine, only: %i[edit update destroy]
  before_action :authorize_users_vaccines

  def index
    @vaccines = Vaccine.all
  end

  def new
    @users_vaccine = Vaccine.new
  end

  def edit; end

  def create
    @users_vaccine = Vaccine.new(users_vaccine_params)

    return redirect_to users_vaccines_url, notice: t('.created') if @users_vaccine.save

    render :new, status: :unprocessable_entity
  end

  def update
    return redirect_to users_vaccines_url, notice: t('.updated') if @users_vaccine.update(users_vaccine_params)

    render :edit, status: :unprocessable_entity
  end

  def destroy
    @users_vaccine.destroy
    redirect_to users_vaccines_url, notice: t('.destroyed')
  end

  private

  def set_users_vaccine
    @users_vaccine = Vaccine.find(params[:id])
  end

  def users_vaccine_params
    params.require(:vaccine).permit(:vaccine_name, :vaccine_status)
  end

  def authorize_users_vaccines
    authorize %i[users vaccines]
  end
end
