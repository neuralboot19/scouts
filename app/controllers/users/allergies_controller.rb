class Users::AllergiesController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :set_users_allergy, only: %i[edit update destroy]
  before_action :authorize_users_allergies

  def index
    @allergies = Allergy.all
  end

  def new
    @users_allergy = Allergy.new
  end

  def edit; end

  def create
    @users_allergy = Allergy.new(users_allergy_params)

    return redirect_to users_allergies_url, notice: t('.created') if @users_allergy.save

    render :new, status: :unprocessable_entity
  end

  def update
    return redirect_to users_allergies_url, notice: t('.updated') if @users_allergy.update(users_allergy_params)

    render :edit, status: :unprocessable_entity
  end

  def destroy
    @users_allergy.destroy
    redirect_to users_allergies_url, notice: t('.destroyed')
  end

  private

  def set_users_allergy
    @users_allergy = Allergy.find(params[:id])
  end

  def users_allergy_params
    params.require(:allergy).permit(:allergies_name, :allergies_status)
  end

  def authorize_users_allergies
    authorize %i[users allergies]
  end
end
