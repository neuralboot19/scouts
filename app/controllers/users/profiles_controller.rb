class Users::ProfilesController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment

  def index
    authorize [:users, :profiles]
    @member = current_user.member
    @medical_files = MedicalFile.find_by_member_id(@member.id)
    @vaccines_member = current_user.member.vaccine_members.active
    @allergies_member = current_user.member.allergy_members.active
    @diseases_member = current_user.member.disease_members.active
    @histories_member = current_user.member.history_members.active
    @handicaps_member = current_user.member.handicap_members.active
    @intolerances_member = current_user.member.intolerance_members.active
  end
end
