require 'xlsxtream'

class Users::ReportsController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment

  def payments
    authorize %i[users reports]
    @member = Member.new
    @regions = nil

    country = Country.all.where(country_default: true, archived: false)
    @regions = Region.all.where(country: country) unless country.nil?
  end

  def members
    authorize %i[users reports]
  end

  def tutors_legals
    authorize %i[users reports]
  end

  def numerical_cutoff
    authorize %i[users reports]
  end

  def my_group
    authorize %i[users reports]
  end

  def my_district
    authorize %i[users reports]
  end

  def my_region
    authorize %i[users reports]
  end

  def reports_by_payments
    authorize %i[users reports]

    # Filtrar pagos con validación
    payments = Payment.where(payment_status: params[:payment_status].to_i) if params[:payment_status].present?
    payments = payments.where(payment_date: params[:filter_date_from]..params[:filter_date_to]) if params[:filter_date_from].present? && params[:filter_date_to].present?

    # Crear un objeto StringIO para almacenar el archivo en memoria
    io = StringIO.new

    # Pasar el objeto StringIO como argumento al constructor Workbook.new
    workbook = Xlsxtream::Workbook.new(io)

    # Escribir en la hoja de cálculo
    workbook.write_worksheet('Sheet1') do |sheet|
      # Agregar encabezados
      sheet << ["Reporte por Pagos"]
      sheet << ["Fecha: #{Time.now.strftime("%Y-%m-%d")}", "Hora: #{Time.now.strftime("%H:%M")}"]
      sheet << ["Nombre miembro", "Apellido miembro", "Cedula", "Fecha de pago", "Monto", "Referencia de pago", "Fecha de vencimiento", "Estatus",  "Unidad", "Grupo", "Distrito", "Region"]
      # Iterar sobre los pagos y escribir los datos en la hoja de cálculo
      payments.each do |payment|
        sheet << [
          payment.user.member&.first_name,
          payment.user.member&.last_name,
          payment.user.member&.cedula,
          payment.payment_date,
          payment.payment_mount,
          payment.payment_reference,
          payment.renewal_end,
          payment.payment_status,
          payment.user.member&.group_unit&.unit&.unit_name,
          payment.user.member&.group_unit&.group&.group_name,
          payment.user.member&.group_unit&.group&.district&.district_name,
          payment.user.member&.group_unit&.group&.district&.region&.region_name,
        ]
      end
    end

    # Cerrar el archivo XLSX
    workbook.close

    # Mover el cursor al principio del objeto StringIO
    io.rewind

    # Enviar los datos del archivo
    send_data io.read, filename: 'report_pagos.xlsx', type: Mime::Type.lookup_by_extension(:xlsx), disposition: 'attachment'
  end

  def reports_by_members
    authorize %i[users reports]

    # Filtrar pagos con validación
    users = User.where(status: params[:status_user])
    # members = Member.where(status_renewal: params[:status_renewal].to_i) if params[:status_renewal].present?
    # members = members.where(is_adult: params[:is_adult]) if params[:is_adult].present?
    # members = members.where(member_type: params[:member_type]) if params[:member_type].present?
    users = users.includes(:member).where(member: { is_adult: params[:is_adult] }) if params[:is_adult].present?
    users = users.includes(:member).where(member: { member_type: params[:member_type] }) if params[:member_type].present?

    # Crear un objeto StringIO para almacenar el archivo en memoria
    io = StringIO.new

    # Pasar el objeto StringIO como argumento al constructor Workbook.new
    workbook = Xlsxtream::Workbook.new(io)

    # Escribir en la hoja de cálculo
    workbook.write_worksheet('Sheet1') do |sheet|
      # Agregar encabezados
      sheet << ["Reporte por miembro"]
      sheet << ["Fecha: #{Time.now.strftime("%Y-%m-%d")}", "Hora: #{Time.now.strftime("%H:%M")}"]
      sheet << ["Cedula", "Primer Nombre", "Segundo Nombre", "Primer Apellido", "Segundo Apellido", "Genero", "Fecha de nacimiento", "Fecha de ingreso", "Fecha promesa", "Nacionalidad", "Unidad", "Adelanto", "Cargo", "Grupo", "Distrito", "Region", "Teléfono", "Correo", "Estado", "Municipio", "Parroquia", "Direccion Completa", "Religion", "Ocupacion", "Lugar de Esudio/Trabajo", "Entidad Publica/Privada", "Nivel de Educacion", "Cargo Institucional", "Region Institucional", "Distrito Institucional", "Adulto/Joven", "Status", "Tipo de miembro", "Correo de Usuario", "Rol"]
      # Iterar sobre los pagos y escribir los datos en la hoja de cálculo
      users.each do |user|
        job_title_member = JobtitleMember.find_by_member_id(user.member.id) if user.member.present?
        job_title_member = "" unless user.member.present?
        job_title_name = job_title_member.jobtitle.jobtitle_name if job_title_member.present?
        job_title_name = "" unless job_title_member.present?
        corporate_member = CorporateMember.find_by_member_id(user.member.id) if user.member.present?
        corporate_name = corporate_member.corporate_jobtitle.corporate_jobtitle_name if corporate_member.present?
        corporate_name = "" unless corporate_member.present?
        member_advance = MemberAdvance.find_by_member_id(user.member.id) if user.member.present?
        advance_name = member_advance.advance.advance_name if member_advance.present?
        advance_name = "" unless member_advance.present?
          sheet << [
          user&.member&.cedula,
          user&.member&.first_name,
          user&.member&.middle_name,
          user&.member&.last_name,
          user&.member&.second_surname,
          user&.member&.gender,
          user&.member&.birthdate,
          user&.member&.date_admission,
          user&.member&.date_promise,
          user&.member&.citizenship,
          user&.member&.group_unit&.unit&.unit_name,
          advance_name,
          job_title_name,
          user&.member&.group_unit&.group&.group_name,
          user&.member&.group_unit&.group&.district&.district_name,
          user&.member&.group_unit&.group&.district&.region&.region_name,
          user&.member&.phone,
          user&.member&.second_email,
          user&.member&.state&.state_name,
          user&.member&.province&.province_name,
          user&.member&.parish&.parishe_name,
          user&.member&.address,
          user&.member&.religion&.religion_name,
          user&.member&.ocupation,
          user&.member&.ocupation_place,
          user&.member&.ocupation_type,
          user&.member&.level_education,
          corporate_name,
          corporate_member&.region&.region_name,
          corporate_member&.district&.district_name,
          user&.member&.is_adult,
          user&.member&.status_renewal,
          user&.member&.member_type,
          user.email,
          user.role.rol_name
        ]
      end
    end

    # Cerrar el archivo XLSX
    workbook.close

    # Mover el cursor al principio del objeto StringIO
    io.rewind

    # Enviar los datos del archivo
    send_data io.read, filename: 'report_miembros.xlsx', type: Mime::Type.lookup_by_extension(:xlsx), disposition: 'attachment'

  end

  def reports_by_tutors_legals
    authorize %i[users reports]

    # Filtrar pagos con validación
    legal_tutor = LegalTutor.all

    # Crear un objeto StringIO para almacenar el archivo en memoria
    io = StringIO.new

    # Pasar el objeto StringIO como argumento al constructor Workbook.new
    workbook = Xlsxtream::Workbook.new(io)

    # Escribir en la hoja de cálculo
    workbook.write_worksheet('Sheet1') do |sheet|
      # Agregar encabezados
      sheet << ["Reporte por Tutor Legar"]
      sheet << ["Fecha: #{Time.now.strftime("%Y-%m-%d")}", "Hora: #{Time.now.strftime("%H:%M")}"]
      sheet << [
        "Cedula",
        "Primer nombre",
        "Segundo nombre",
        "Primer apellido",
        "Segundo apellido",
        "Genero",
        "Estado",
        "Municipio",
        "Parroquia",
        "Dirección",
        "Nivel de Educación",
        "Ocupación",
        "Sitio de trabajo/estudio",
        "Privado/Publico",
        "Teléfono",
        "Fecha de nacimiento",
        "Correo",
        "Nacionalidad",
        "Religión",
        "Cedula del representado",
        "Nombre del representado",
        "Apellido del representado",
        "Unidad",
        "Grupo",
        "Distrito",
        "Region"
      ]

      # Iterar sobre los pagos y escribir los datos en la hoja de cálculo
      legal_tutor.each do |tutor|
        sheet << [
          tutor.cedula,
          tutor.first_name,
          tutor.middle_name,
          tutor.last_name,
          tutor.second_surname,
          tutor.gender,
          tutor.state&.state_name,
          tutor.province&.province_name,
          tutor.parish&.parishe_name,
          tutor.address,
          tutor.level_education,
          tutor.ocupation,
          tutor.ocupation_place,
          tutor.ocupation_type,
          tutor.phone,
          tutor.birthdate,
          tutor.second_email,
          tutor.citizenship,
          tutor.religion&.religion_name,
          tutor.member&.cedula,
          tutor.member&.first_name,
          tutor.member&.last_name,
          tutor.member&.group_unit&.unit&.unit_name,
          tutor.member&.group_unit&.group&.group_name,
          tutor.member&.group_unit&.group&.district&.district_name,
          tutor.member&.group_unit&.group&.district&.region&.region_name,
        ]
      end
    end

    # Cerrar el archivo XLSX
    workbook.close

    # Mover el cursor al principio del objeto StringIO
    io.rewind

    # Enviar los datos del archivo
    send_data io.read, filename: 'report_by_tutor_legal.xlsx', type: Mime::Type.lookup_by_extension(:xlsx), disposition: 'attachment'
  end

  def reports_by_numerical_cutoff
    authorize %i[users reports]

    # Obtener todas las unidades
    units = Unit.all

    # Obtener nombre de unidades para encabezado
    unit_attributes = units.pluck(:unit_name)

    # Crear un objeto StringIO para almacenar el archivo en memoria
    io = StringIO.new

    # Pasar el objeto StringIO como argumento al constructor Workbook.new
    workbook = Xlsxtream::Workbook.new(io)

    # Crear un hash para almacenar el recuento de miembros por cada unidad y grupo
    unit_member_counts = {}

    # Iterar sobre cada unidad
    units.each do |unit|
      # Obtener los group_units para la unidad actual
      group_units = unit.group_units

      # Iterar sobre cada group_unit y contar los miembros por grupo
      group_units.each do |group_unit|
        # Obtener el nombre del grupo
        group_name = group_unit.group.group_name
        district = group_unit.group.district.district_name
        region = group_unit.group.district.region.region_name

        # Creamos un identificador único para el grupo concatenando el nombre del grupo, el distrito y la región
        group_identifier = "#{group_name} - #{district} - #{region}"

        # Inicializar el contador de miembros para este grupo
        member_count = 0
        group_unit.members.each do |member|
          u = User.where(member: member, status: 1)
          member_count = member_count + 1 if u.present?
        end

        # Inicializar el hash para el grupo si aún no existe
        unit_member_counts[group_identifier] ||= {}

        # Almacenar el recuento de miembros para este grupo en el hash
        unit_member_counts[group_identifier][unit.unit_name] = { member_count: member_count, district: district, region: region }
      end
    end

    # Escribir en la hoja de cálculo
    workbook.write_worksheet('Sheet1') do |sheet|
      # Agregar encabezados
      sheet << ["Fecha: #{Time.now.strftime("%Y-%m-%d")}", "Hora: #{Time.now.strftime("%H:%M")}"]
      sheet << ["Grupo", "Distrito", "Región"] + unit_attributes

      # Iterar sobre cada grupo y escribir los datos en la hoja de cálculo
      unit_member_counts.each do |group_name, data|
        # Crear una fila para el grupo
        row = [group_name.split(' - ').first, data.values.first[:district], data.values.first[:region]]

        # Iterar sobre cada unidad y agregar el recuento de miembros correspondiente al grupo
        unit_attributes.each do |unit_name|
          # Obtener el recuento de miembros, el distrito y la región para este grupo y unidad
          member_count = data[unit_name][:member_count] || 0

          # Agregar el recuento de miembros a la fila
          row << member_count
        end

        # Agregar la fila completa a la hoja de cálculo
        sheet << row
      end
    end

    # Cerrar el archivo XLSX
    workbook.close

    # Mover el cursor al principio del objeto StringIO
    io.rewind

    # Enviar los datos del archivo
    send_data io.read, filename: 'numerical_cutoff.xlsx', type: Mime::Type.lookup_by_extension(:xlsx), disposition: 'attachment'
  end

  def reports_by_my_group
    authorize %i[users reports]

    # Encuentra el miembro del current_user
    member = current_user.member

    # Encuentra el grupo del miembro
    group = member.group_unit.group

    # Encuentra todas las unidades asociadas al grupo
    unidades_del_grupo = Unit.joins(:group_units).where(group_units: { group_id: group.id })

    miembros_activos = []
    unidades_del_grupo.each do |unidad|
      miembros_unidad = Member.joins(group_unit: :group).where(group_units: { unit_id: unidad.id, group_id: group.id })

      # Inicializar la busqueda de miembros con estatus activos del modelo user
      miembros_unidad.each do |member|
        u = User.where(member: member, status: 1)
        miembros_activos << u if u.present?
      end
    end

    # Crear un objeto StringIO para almacenar el archivo en memoria
    io = StringIO.new

    # Pasar el objeto StringIO como argumento al constructor Workbook.new
    workbook = Xlsxtream::Workbook.new(io)

    # Escribir en la hoja de cálculo
    workbook.write_worksheet('Sheet1') do |sheet|
      # Agregar encabezados
      sheet << ["Reporte de mi grupo"]
      sheet << ["Fecha: #{Time.now.strftime("%Y-%m-%d")}", "Hora: #{Time.now.strftime("%H:%M")}"]
      sheet << ["Nombre miembro", "Apellido miembro", "Cedula miembro", "Fecha de vencimiento", "Unidad", "Cargo", "Nombre representante", "Apellido representante", "Cedula representante", "Telefono representante", "Adulto/Joven"]

      # Iterar sobre los pagos y escribir los datos en la hoja de cálculo
      miembros_activos.each do |user|
        renewal_end = user[0].payments.where(payment_status: 1).last
        renewal_end = renewal_end&.renewal_end
        renewal_end = "" unless renewal_end.present?
        lt_first_name = LegalTutor.find_by_member_id(user[0].member.id) if user[0].member.present?
        lt_first_name = lt_first_name&.first_name
        lt_first_name = "" unless lt_first_name.present?
        lt_last_name = LegalTutor.find_by_member_id(user[0].member.id) if user[0].member.present?
        lt_last_name = lt_last_name&.last_name
        lt_last_name = "" unless lt_last_name.present?
        lt_cedula = LegalTutor.find_by_member_id(user[0].member.id) if user[0].member.present?
        lt_cedula = lt_cedula&.cedula
        lt_cedula = "" unless lt_cedula.present?
        lt_phone = LegalTutor.find_by_member_id(user[0].member.id) if user[0].member.present?
        lt_phone = lt_phone&.phone
        lt_phone = "" unless lt_phone.present?
        search_jm = user[0]&.member&.jobtitle_members.last
        job_name = ""
        if search_jm.present?
          job_name = search_jm&.jobtitle&.jobtitle_name
        end
        is_adult = user[0]&.member&.is_adult

        sheet << [
          user[0]&.member&.first_name,
          user[0]&.member&.last_name,
          user[0]&.member&.cedula,
          renewal_end,
          user[0]&.member&.group_unit&.unit&.unit_name,
          job_name,
          lt_first_name,
          lt_last_name,
          lt_cedula,
          lt_phone,
          is_adult,
        ]
      end
    end

    # Cerrar el archivo XLSX
    workbook.close

    # Mover el cursor al principio del objeto StringIO
    io.rewind

    # Enviar los datos del archivo
    send_data io.read, filename: 'my_group.xlsx', type: Mime::Type.lookup_by_extension(:xlsx), disposition: 'attachment'
  end

  def reports_by_my_district
    authorize %i[users reports]

    # Encuentra el miembro corporate del current_user
    corporate = CorporateMember.find_by_member_id(current_user.member.id)

    # Encuentra el distrito del miembro corporate
    district = corporate.district

    # Encuentra todos los grupos asociados al distrito
    groups_in_district = Group.where(district_id: district.id)

    # Inicializa un arreglo para almacenar todos los miembros activos
    miembros_activos = []

    # Itera sobre cada grupo del distrito
    groups_in_district.each do |group|

      # Encuentra todas las unidades asociadas al grupo
      unidades_del_grupo = Unit.joins(:group_units).where(group_units: { group_id: group.id })

      # Itera sobre cada unidad del grupo
      unidades_del_grupo.each do |unidad|
        # Encuentra todos los miembros de la unidad actual
        miembros_unidad = Member.joins(group_unit: :group).where(group_units: { unit_id: unidad.id, group_id: group.id })

        # Inicializar la busqueda de miembros con estatus activos del modelo user
        miembros_unidad.each do |member|
          u = User.where(member: member, status: 1)
          miembros_activos << [u, group] if u.present?
        end
      end
    end

    # Crear un objeto StringIO para almacenar el archivo en memoria
    io = StringIO.new

    # Pasar el objeto StringIO como argumento al constructor Workbook.new
    workbook = Xlsxtream::Workbook.new(io)

    # Escribir en la hoja de cálculo
    workbook.write_worksheet('Sheet1') do |sheet|
      # Agregar encabezados
      sheet << ["Reporte de miembros del distrito #{district.district_name}"]
      sheet << ["Fecha: #{Time.now.strftime("%Y-%m-%d")}", "Hora: #{Time.now.strftime("%H:%M")}"]
      sheet << ["Nombre miembro", "Apellido miembro", "Cedula miembro", "Fecha de vencimiento", "Unidad", "Nombre representante", "Apellido representante", "Cedula representante", "Telefono representante", "Grupo"]

      # Iterar sobre los pagos y escribir los datos en la hoja de cálculo
      miembros_activos.each do |user|
        us = user[0].last
        renewal_end = us.payments.where(payment_status: 1).last
        renewal_end = renewal_end&.renewal_end
        renewal_end = "" unless renewal_end.present?
        lt_first_name = LegalTutor.find_by_member_id(us.member.id) if us.member.present?
        lt_first_name = lt_first_name&.first_name
        lt_first_name = "" unless lt_first_name.present?
        lt_last_name = LegalTutor.find_by_member_id(us.member.id) if us.member.present?
        lt_last_name = lt_last_name&.last_name
        lt_last_name = "" unless lt_last_name.present?
        lt_cedula = LegalTutor.find_by_member_id(us.member.id) if us.member.present?
        lt_cedula = lt_cedula&.cedula
        lt_cedula = "" unless lt_cedula.present?
        lt_phone = LegalTutor.find_by_member_id(us.member.id) if us.member.present?
        lt_phone = lt_phone&.phone
        lt_phone = "" unless lt_phone.present?

        sheet << [
          us&.member&.first_name,
          us&.member&.last_name,
          us&.member&.cedula,
          renewal_end,
          us&.member&.group_unit&.unit&.unit_name,
          lt_first_name,
          lt_last_name,
          lt_cedula,
          lt_phone,
          user[1]&.group_name,
        ]
      end
    end

    # Cerrar el archivo XLSX
    workbook.close

    # Mover el cursor al principio del objeto StringIO
    io.rewind

    # Enviar los datos del archivo
    send_data io.read, filename: 'my_distric.xlsx', type: Mime::Type.lookup_by_extension(:xlsx), disposition: 'attachment'
  end

  def reports_by_my_region
    authorize %i[users reports]

    # Encuentra el miembro corporate del current_user
    corporate = CorporateMember.find_by_member_id(current_user.member.id)

    # Encuentra la región del miembro corporate
    region = corporate.region

    # Encuentra los distritos de la región
    districts_in_region = District.where(region_id: region.id)

    # Inicializa un arreglo para almacenar todos los miembros activos por región
    miembros_activos_por_region = []

    # Itera sobre cada distrito de la región
    districts_in_region.each do |district|
      # Encuentra todos los grupos asociados al distrito
      groups_in_district = Group.where(district_id: district.id)

      # Itera sobre cada grupo del distrito
      groups_in_district.each do |group|
        # Encuentra todas las unidades asociadas al grupo
        unidades_del_grupo = Unit.joins(:group_units).where(group_units: { group_id: group.id })

        # Itera sobre cada unidad del grupo
        unidades_del_grupo.each do |unidad|
          # Encuentra todos los miembros de la unidad actual
          miembros_unidad = Member.joins(group_unit: :group).where(group_units: { unit_id: unidad.id, group_id: group.id })

          # Inicializar la búsqueda de miembros con estatus activos del modelo user
          miembros_unidad.each do |member|
            u = User.where(member: member, status: 1)
            miembros_activos_por_region << [u, group, district] if u.present?
          end
        end
      end
    end

    # Crear un objeto StringIO para almacenar el archivo en memoria
    io = StringIO.new

    # Pasar el objeto StringIO como argumento al constructor Workbook.new
    workbook = Xlsxtream::Workbook.new(io)

    # Escribir en la hoja de cálculo
    workbook.write_worksheet('Sheet1') do |sheet|
      # Agregar encabezados
      sheet << ["Reporte de miembros del distrito #{region.region_name}"]
      sheet << ["Fecha: #{Time.now.strftime("%Y-%m-%d")}", "Hora: #{Time.now.strftime("%H:%M")}"]
      sheet << ["Nombre miembro", "Apellido miembro", "Cedula miembro", "Fecha de vencimiento", "Unidad", "Nombre representante", "Apellido representante", "Cedula representante", "Telefono representante", "Grupo", "Distrito"]

      # Iterar sobre los pagos y escribir los datos en la hoja de cálculo
      miembros_activos_por_region.each do |user|
        us = user[0].last
        renewal_end = us.payments.where(payment_status: 1).last
        renewal_end = renewal_end&.renewal_end
        renewal_end = "" unless renewal_end.present?
        lt_first_name = LegalTutor.find_by_member_id(us.member.id) if us.member.present?
        lt_first_name = lt_first_name&.first_name
        lt_first_name = "" unless lt_first_name.present?
        lt_last_name = LegalTutor.find_by_member_id(us.member.id) if us.member.present?
        lt_last_name = lt_last_name&.last_name
        lt_last_name = "" unless lt_last_name.present?
        lt_cedula = LegalTutor.find_by_member_id(us.member.id) if us.member.present?
        lt_cedula = lt_cedula&.cedula
        lt_cedula = "" unless lt_cedula.present?
        lt_phone = LegalTutor.find_by_member_id(us.member.id) if us.member.present?
        lt_phone = lt_phone&.phone
        lt_phone = "" unless lt_phone.present?

        sheet << [
          us&.member&.first_name,
          us&.member&.last_name,
          us&.member&.cedula,
          renewal_end,
          us&.member&.group_unit&.unit&.unit_name,
          lt_first_name,
          lt_last_name,
          lt_cedula,
          lt_phone,
          user[1]&.group_name,
          user[2]&.district_name,
        ]
      end
    end

    # Cerrar el archivo XLSX
    workbook.close

    # Mover el cursor al principio del objeto StringIO
    io.rewind

    # Enviar los datos del archivo
    send_data io.read, filename: 'my_region.xlsx', type: Mime::Type.lookup_by_extension(:xlsx), disposition: 'attachment'
  end

end
