class Users::DiseasesController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :set_users_disease, only: %i[edit update destroy]
  before_action :authorize_users_diseases

  def index
    @diseases = Disease.all
  end

  def new
    @users_disease = Disease.new
  end

  def edit; end

  def create
    @users_disease = Disease.new(users_disease_params)

    return redirect_to users_diseases_url, notice: t('.created') if @users_disease.save

    render :new, status: :unprocessable_entity
  end

  def update
    return redirect_to users_diseases_url, notice: t('.updated') if @users_disease.update(users_disease_params)

    render :edit, status: :unprocessable_entity
  end

  def destroy
    @users_disease.destroy
    redirect_to users_diseases_url, notice: t('.destroyed')
  end

  private

  def set_users_disease
    @users_disease = Disease.find(params[:id])
  end

  def users_disease_params
    params.require(:disease).permit(:diseases_name, :diseases_status)
  end

  def authorize_users_diseases
    authorize %i[users diseases]
  end
end
