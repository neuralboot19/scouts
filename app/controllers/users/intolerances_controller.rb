class Users::IntolerancesController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :set_users_intolerance, only: %i[edit update destroy]
  before_action :authorize_users_intolerances

  def index
    @intolerances = Intolerance.all
  end

  def new
    @users_intolerance = Intolerance.new
  end

  def edit; end

  def create
    @users_intolerance = Intolerance.new(users_intolerance_params)

    return redirect_to users_intolerances_url, notice: t('.created') if @users_intolerance.save

    render :new, status: :unprocessable_entity
  end

  def update
    return redirect_to users_intolerances_url, notice: t('.updated') if @users_intolerance.update(users_intolerance_params)

    render :edit, status: :unprocessable_entity
  end

  def destroy
    @users_intolerance.destroy
    redirect_to users_intolerances_url, notice: t('.destroyed')
  end

  private

  def set_users_intolerance
    @users_intolerance = Intolerance.find(params[:id])
  end

  def users_intolerance_params
    params.require(:intolerance).permit(:intolerances_name, :intolerances_status)
  end

  def authorize_users_intolerances
    authorize %i[users intolerances]
  end
end

# FRANCISCO JAVIER VALLECILLO
# 900535025
# sociasysocios.es@greenpas.org
