class Users::HandicapsController < ApplicationController
  include VerificateControllerConcern

  before_action :validate_payment
  before_action :set_users_handicap, only: %i[edit update destroy]
  before_action :authorize_users_handicaps

  def index
    @handicaps = Handicap.all
  end

  def new
    @users_handicap = Handicap.new
  end

  def edit; end

  def create
    @users_handicap = Handicap.new(users_handicap_params)

    return redirect_to users_handicaps_url, notice: t('.created') if @users_handicap.save

    render :new, status: :unprocessable_entity
  end

  def update
    return redirect_to users_handicaps_url, notice: t('.updated') if @users_handicap.update(users_handicap_params)

    render :edit, status: :unprocessable_entity
  end

  def destroy
    @users_handicap.destroy
    redirect_to users_handicaps_url, notice: t('.destroyed')
  end

  private

  def set_users_handicap
    @users_handicap = Handicap.find(params[:id])
  end

  def users_handicap_params
    params.require(:handicap).permit(:handicaps_name, :handicaps_status)
  end

  def authorize_users_handicaps
    authorize %i[users handicaps]
  end
end
