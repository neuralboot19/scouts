class MembersController < ApplicationController
  def index
    authorize %i[members]
    @member = Member.new
  end

  def new
    authorize %i[members]
    @cedula = session[:cedula]
    @member = Member.new
    @states = nil

    country = Country.all.where(country_default: true, archived: false)
    @states = State.all.where(country: country) unless country.nil?
  end

  def create
    authorize %i[members]
    data_member = member_params
    data_member[:users] = [current_user]
    country = Country.find_by_id(data_member[:citizenship])
    data_member[:citizenship] = country.country_type
    @member = Member.new(data_member)

    current_date = Date.today
    age = current_date.year - Date.parse(data_member[:birthdate]).year
    age -= 1 if current_date < @member.birthdate + age.years
    @member.age = age

    if data_member[:member_type] == "dualidad" || data_member[:member_type] == "institucional"
      @member.is_adult = 0
    end

    if @member.save
      session[:member] = @member.id
      set_member(@member, age)
    else
      render :new, status: :unprocessable_entity, alert: 'No cumples con el requerimiento necesario.'
    end
  end

  def edit
    authorize %i[members]
    @member = Member.find(session[:member])
    session[:member] = @member.id
    @states = nil

    country = Country.all.where(country_default: true, archived: false)
    @states = State.all.where(country: country) unless country.nil?
  end

  def update
    authorize %i[members]
    data_member = member_params
    country = Country.find_by_id(data_member[:citizenship])
    data_member[:citizenship] = country.country_type

    @member = Member.find(session[:member])
    @member.state_id = data_member[:state_id]

    current_date = Date.today
    age = current_date.year - Date.parse(data_member[:birthdate]).year
    age -= 1 if current_date < @member.birthdate + age.years
    @member.age = age

    if @member.update(data_member)
      session[:member] = @member.id
      set_member(@member, age)
    else
      render :edit, status: :unprocessable_entity
    end
  end

  def verificate
    authorize %i[members]
    @cedula = cedula_params[:cedula]

    @member = session[:member]
    session[:cedula] = @cedula
    @usuario = Member.find_by_cedula(@cedula)
    if Member.exists?(cedula: @cedula)
      redirect_to members_path, alert: 'Ya existe un usuario con esta cedula'
    elsif @usuario && @member.present?
      redirect_to edit_member_path
    else
      redirect_to new_member_path
    end
  end

  def grupo
    authorize %i[members]
    @cedula = session[:cedula]
    @member = Member.new
    @regions = nil

    country = Country.all.where(country_default: true, archived: false)
    @regions = Region.all.where(country: country) unless country.nil?
  end

  def regions
    authorize %i[members]
    @target_d = params[:target_d]
    @target_g = params[:target_g]
    @target_u = params[:target_u]
    @country = Country.all.where(country_default: true, archived: false)

    @regions = ['Selecciona una region']
    @regions.concat(@country.regions.pluck(:region_name)) unless @country.nil?
    @districts = ['Selecciona un distrito']
    @group_units = ['Selecciona un grupo']
    @units = ['Selecciona una unidad']

    respond_to do |format|
      format.turbo_stream {}
    end
  end

  def districts
    authorize %i[members]
    @target_d = params[:target_d]
    @target_g = params[:target_g]
    @target_u = params[:target_u]
    @region = Region.find_by_id(params[:region])
    @districts = ['Selecciona un distrito']
    @group_units = ['Selecciona un grupo']
    @units = ['Selecciona una unidad']
    @districts.concat(@region.districts.pluck(:district_name, :id).sort_by(&:first)) unless @region.nil?

    respond_to do |format|
      format.turbo_stream {}
    end
  end

  def groups
    authorize %i[members]
    @target_g = params[:target_g]
    @target_u = params[:target_u]
    @district = District.find_by_id(params[:district])
    @selected_district_groups = @district.groups
    @group_units = ['Selecciona un grupo']
    @units = ['Selecciona una unidad']
    @selected_district_groups.each do |group|
      search = GroupUnit.where(group: group)
      group_search = search.map { |g| group_name = g.group.group_name, id = g.group.id }
      @group_units.concat(group_search)
    end

    @group_units.uniq!

    respond_to do |format|
      format.turbo_stream {}
    end
  end

  def units
    authorize %i[members]
    @member = Member.find_by_id(session[:member])
    @target_u = params[:target_u]
    @group = Group.find_by_id(params[:group])
    group_units = GroupUnit.where(group_id: @group.id)
    @units = ['Selecciona una unidad']
    @units = [] if @member.is_adult == "youth"
    group_units.each do |gu|
      search = Unit.where(id: gu.unit_id)
      search_unit = search.map { |u| unit_name = u.unit_name, id = u.id }
      @units.concat(search_unit)
    end

    if @member.is_adult == "youth"
      @units.shift[0] if @units[0][0] == "Conductor de Grupo"
      @units.unshift(["Selecciona una unidad"])
    end

    @units.uniq!

    respond_to do |format|
      format.turbo_stream {}
    end
  end

  def advance
    authorize %i[members]
    @advanceA_ID = params[:advanceA]
    @unit = Unit.find_by_id(params[:unit])
    @advances = ['Selecciona un adelanto']
    @advances.concat(@unit.advances.pluck(:advance_name, :id).sort_by(&:first)) unless @unit.nil?

    respond_to do |format|
      format.turbo_stream {}
    end
  end

  def states
    authorize %i[members]
    @target_p = params[:target_p]
    @target_h = params[:target_h]
    @provinces = ['Selecciona un municipio']
    @parishes = ['Selecciona una parroquia']

    @state = State.find_by_id(params[:state])
    @provinces.concat(@state.provinces.pluck(:province_name, :id).sort_by(&:first)) unless @state.nil?

    respond_to do |format|
      format.turbo_stream {}
    end
  end

  def provinces
    authorize %i[members]
    @target_h = params[:target_h]
    @province = Province.find_by_id(params[:province])
    @parishes = ['Selecciona una parroquia']
    @parishes.concat(@province.parishes.pluck(:parishe_name, :id).sort_by(&:first)) unless @province.nil?

    respond_to do |format|
      format.turbo_stream {}
    end
  end

  def grupo_submit
    authorize %i[members]
    member = Member.find(session[:member])
    group = Group.find_by_id(grupo_params[:group])
    unit = Unit.find_by_id(grupo_params[:unit])
    group_unit = GroupUnit.where(unit_id:unit.id, group_id:group.id).last if group.present? && unit.present?
    jobtitle = Jobtitle.find_by_id(grupo_params[:job_title_id])
    jobtitle_member = JobtitleMember.find_or_create_by(jobtitle_id: jobtitle.id, member_id: member.id)
    if member.update!(group_unit: group_unit, date_admission: grupo_params[:date_admission] , date_promise: grupo_params[:date_promise]) && group
      redirect_to authenticated_root_path, notice: 'Usuario configurado correctamente'
    else
      redirect_to members_grupo_path, alert: 'Se requiere un grupo'
    end
  end

  def dualidad
    authorize %i[members]
    @cedula = session[:cedula]
    @member = Member.new
    @regions = nil

    country = Country.all.where(country_default: true, archived: false)
    @regions = Region.all.where(country: country) unless country.nil?
  end

  def dualidad_submit
    authorize %i[members]
    member = Member.find(session[:member])
    region = Region.find_by_id(dualidad_params[:region])
    district = District.find_by_id(dualidad_params[:district])
    group = Group.find_by_id(dualidad_params[:group])
    unit = Unit.find_by_id(dualidad_params[:unit])
    group_unit = GroupUnit.where(unit_id:unit.id, group_id:group.id).last if group.present? && unit.present?
    jobtitle = Jobtitle.find_by_id(dualidad_params[:job_title_id])
    jobtitle_member = JobtitleMember.find_or_create_by(jobtitle_id: jobtitle.id, member_id: member.id)
    corporate_jobtitle = CorporateJobtitle.find_by_id(dualidad_params[:corporate_jobtitles_id])
    corporate_members = CorporateMember.find_or_create_by(member_id: member.id, corporate_jobtitle: corporate_jobtitle, region_id: region.id, district_id: district.id)
    if member.update!(group_unit: group_unit, date_admission: dualidad_params[:date_admission] , date_promise: dualidad_params[:date_promise]) && group && jobtitle_member && corporate_members
      redirect_to authenticated_root_path, notice: 'Usuario configurado correctamente'
    else
      redirect_to members_grupo_path, alert: 'se requiere un grupo un cargo y cargo institucional.'
    end
  end

  def institucional
    authorize %i[members]
    @cedula = session[:cedula]
    @member = Member.find(session[:member])
    @regions = nil

    country = Country.all.where(country_default: true, archived: false)
    @regions = Region.all.where(country: country) unless country.nil?
  end

  def institucional_submit
    authorize %i[members]
    member = Member.find(session[:member])
    region = Region.find_by_id(institucional_params[:region])
    district = District.find_by_id(institucional_params[:district])
    corporate_jobtitle = CorporateJobtitle.find_by_id(institucional_params[:corporate_jobtitles_id])
    corporate_members = CorporateMember.find_or_create_by(member_id: member.id, corporate_jobtitle: corporate_jobtitle, region_id: region.id, district_id: district.id)
    if member.update!(date_admission: institucional_params[:date_admission] , date_promise: institucional_params[:date_promise]) && corporate_members
      redirect_to authenticated_root_path, notice: 'Usuario configurado correctamente'
    else
      redirect_to members_grupo_path, alert: 'se requiere un grupo un cargo y cargo institucional.'
    end
  end

  def legal_tutor
    authorize %i[members]
    @cedula = session[:cedula]
    @member = Member.find(session[:member])
    @legal_tutor = LegalTutor.new
    @states = nil

    country = Country.all.where(country_default: true, archived: false)
    @states = State.all.where(country: country) unless country.nil?
  end

  def legal_tutor_submit
    authorize %i[members]
    @member = Member.find_by_id(session[:member])
    data_legal_tutor = legal_tutor_params
    country = Country.find_by_id(data_legal_tutor[:citizenship])
    data_legal_tutor[:citizenship] = country.country_type.to_s
    data_legal_tutor[:member_id] = @member.id
    @legal_tutor = LegalTutor.new(data_legal_tutor)
    current_date = Date.today
    age = current_date.year - Date.parse(data_legal_tutor[:birthdate]).year
    age -= 1 if current_date < @legal_tutor.birthdate + age.years
    @legal_tutor.age = age

    if @legal_tutor.save
      session[:member] = @member.id
      redirect_to members_group_youth_path
    else
      render :new, status: :unprocessable_entity, alert: 'No cumples con el requerimiento necesario.'
    end
  end

  def group_youth
    authorize %i[members]
    @cedula = session[:cedula]
    @member = Member.new
    @regions = nil

    country = Country.all.where(country_default: true, archived: false)
    @regions = Region.all.where(country: country) unless country.nil?
  end

  def group_youth_submit
    authorize %i[members]
    member = Member.find(session[:member])
    advance = Advance.find_by_id(group_youth_params[:advance])
    group = Group.find_by_id(group_youth_params[:group])
    unit = Unit.find_by_id(group_youth_params[:unit_id])
    group_unit = GroupUnit.where(unit_id:unit.id, group_id:group.id).last if group.present? && unit.present?
    member_advance = MemberAdvance.create(member_id: member.id, advance_id: advance.id)

    if member.update!(group_unit: group_unit, date_admission: group_youth_params[:date_admission] , date_promise: group_youth_params[:date_promise]) && group && member_advance
      redirect_to authenticated_root_path, notice: 'Usuario configurado correctamente'
    else
      redirect_to members_grupo_path, alert: 'se requiere un grupo'
    end
  end

  private

  def set_member(member, age)
    if age >= 18
      puts "El Usuario es mayor de 18 años."
      if member.member_type == "grupo"
        if member.is_adult == "adult"
          redirect_to members_grupo_path
        elsif member.is_adult == "youth"
          redirect_to members_group_youth_path
        end
      elsif member.member_type == "dualidad"
        redirect_to members_dualidad_path
      elsif member.member_type == "institucional"
        redirect_to members_institucional_path
      end
    elsif age < 18
      puts "El Usuario es menor de 18 años."
      redirect_to members_legal_tutor_path
    end
  end

  def cedula_params
    params.require(:member).permit(:cedula)
  end

  def grupo_params
    params.require(:member).permit(:region, :district, :group, :job_title_id, :unit, :date_admission, :date_promise)
  end

  def group_youth_params
    params.require(:member).permit(:region, :district, :group, :unit_id, :advance, :date_admission, :date_promise)
  end

  def dualidad_params
    params.require(:member).permit(:region, :district, :group, :job_title_id, :unit, :corporate_jobtitles_id, :date_admission, :date_promise)
  end

  def institucional_params
    params.require(:member).permit(:region, :district, :group, :unit_id, :corporate_jobtitles_id, :date_admission, :date_promise)
  end

  def member_params
    params.require(:member).permit(:cedula, :first_name, :middle_name, :last_name, :second_surname, :gender, :citizenship, :state_id, :province_id, :parish_id, :religion_id, :address, :ocupation, :ocupation_place, :ocupation_type, :phone, :level_education, :age, :birthdate, :member_type, :is_adult, :second_email)
  end

  def legal_tutor_params
    params.require(:legal_tutor).permit(:cedula, :first_name, :middle_name, :last_name, :second_surname, :gender, :citizenship, :state_id, :province_id, :parish_id, :religion_id, :address, :ocupation, :ocupation_place, :ocupation_type, :phone, :level_education, :age, :birthdate, :second_email)
  end
end
