class PagesPolicy < ApplicationPolicy
  def index?
    user.can_any? :pages_index, :superadmin
  end
end
