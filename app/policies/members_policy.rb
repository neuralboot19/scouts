class MembersPolicy < ApplicationPolicy
  def index?
    user.can_any? :members_index, :superadmin
  end

  def new?
    true
  end

  def edit?
    true
  end

  def create?
    true
  end

  def grupo?
    true
  end

  def grupo_submit?
    true
  end

  def dualidad?
    true
  end

  def dualidad_submit?
    true
  end

  def verificate?
    true
  end

  def regions?
    true
  end

  def districts?
    true
  end

  def groups?
    true
  end

  def institucional?
    true
  end

  def institucional_submit?
    true
  end

  def legal_tutor?
    true
  end

  def legal_tutor_submit?
    true
  end

  def group_youth?
    true
  end

  def group_youth_submit?
    true
  end

  def advance?
    true
  end

  def units?
    true
  end

  def update?
    true
  end

  def states?
    true
  end

  def provinces?
    true
  end
end
