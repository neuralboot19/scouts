class Users::MedicalFilesPolicy < ApplicationPolicy
  # NOTE: Up to Pundit v2.3.1, the inheritance was declared as
  # `Scope < Scope` rather than `Scope < ApplicationPolicy::Scope`.
  # In most cases the behavior will be identical, but if updating existing
  # code, beware of possible changes to the ancestors:
  # https://gist.github.com/Burgestrand/4b4bc22f31c8a95c425fc0e30d7ef1f5

  class Scope < ApplicationPolicy::Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def new?
    user.can_any? :medical_files_new, :superadmin
  end

  def edit?
    user.can_any? :medical_files_edit, :superadmin
  end

  def create?
    user.can_any? :medical_files_create, :superadmin
  end

  def update?
    user.can_any? :medical_files_update, :superadmin
  end
end
