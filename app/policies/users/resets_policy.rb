class Users::ResetsPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def resets_nav?
    user.can_any? :resets_nav, :superadmin
  end

  def group?
    user.can_any? :resets_group, :superadmin
  end

  def district?
    user.can_any? :resets_district, :superadmin
  end

  def region?
    user.can_any? :resets_region, :superadmin
  end

  def update?
    user.can_any? :resets_update, :superadmin
  end
end
