class Users::PaymentsPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def index?
    user.can_any? :payments_index, :superadmin
  end

  def new?
    user.can_any? :payments_new, :superadmin
  end

  def create?
    user.can_any? :payments_create, :superadmin
  end

  def destroy?
    user.can_any? :payments_destroy, :superadmin
  end

  def show?
    user.can_any? :payments_show, :superadmin
  end

  def checkings?
    user.can_any? :payments_checkings, :superadmin
  end

  def swap?
    user.can_any? :payments_swap, :superadmin
  end
end
