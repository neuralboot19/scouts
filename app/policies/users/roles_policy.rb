class Users::RolesPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def roles_nav?
    user.can_any? :roles_nav, :superadmin
  end

  def index?
    user.can_any? :roles_index, :superadmin
  end

  def new?
    user.can_any? :roles_new, :superadmin
  end

  def edit?
    user.can_any? :roles_edit, :superadmin
  end

  def create?
    user.can_any? :roles_create, :superadmin
  end

  def update?
    user.can_any? :roles_update, :superadmin
  end

  def destroy?
    user.can_any? :roles_destroy, :superadmin
  end
end
