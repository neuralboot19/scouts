class Users::AmountsPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def index?
    user.can_any? :amounts_index, :superadmin
  end

  def show?
    user.can_any? :amounts_show, :superadmin
  end

  def new?
    user.can_any? :amounts_new, :superadmin
  end

  def edit?
    user.can_any? :amounts_edit, :superadmin
  end

  def create?
    user.can_any? :amounts_create, :superadmin
  end

  def update?
    user.can_any? :amounts_update, :superadmin
  end

  def destroy?
    user.can_any? :amounts_destroy, :superadmin
  end
end
