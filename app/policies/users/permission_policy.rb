class Users::PermissionPolicy < ApplicationPolicy
  class Scope < Scope
    def resolve
      if user.can_any?(:superadmin)
        scope.all
      else
        scope.where.not(permission_type: :superadmin)
      end
    end
  end
end
