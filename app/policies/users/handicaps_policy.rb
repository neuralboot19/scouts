class Users::HandicapsPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def index?
    user.can_any? :handicaps_index, :superadmin
  end

  def new?
    user.can_any? :handicaps_new, :superadmin
  end

  def edit?
    user.can_any? :handicaps_edit, :superadmin
  end

  def create?
    user.can_any? :handicaps_create, :superadmin
  end

  def update?
    user.can_any? :handicaps_update, :superadmin
  end

  def destroy?
    user.can_any? :handicaps_destroy, :superadmin
  end
end
