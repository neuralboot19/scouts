class Users::HistoriesPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def index?
    user.can_any? :histories_index, :superadmin
  end

  def new?
    user.can_any? :histories_new, :superadmin
  end

  def edit?
    user.can_any? :histories_edit, :superadmin
  end

  def create?
    user.can_any? :histories_create, :superadmin
  end

  def update?
    user.can_any? :histories_update, :superadmin
  end

  def destroy?
    user.can_any? :histories_destroy, :superadmin
  end
end
