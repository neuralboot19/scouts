class Users::VaccinesPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def medicals_nav?
    user.can_any? :medicals_nav, :superadmin
  end

  def index?
    user.can_any? :vaccines_index, :superadmin
  end

  def new?
    user.can_any? :vaccines_new, :superadmin
  end

  def edit?
    user.can_any? :vaccines_edit, :superadmin
  end

  def create?
    user.can_any? :vaccines_create, :superadmin
  end

  def update?
    user.can_any? :vaccines_update, :superadmin
  end

  def destroy?
    user.can_any? :vaccines_destroy, :superadmin
  end
end
