class Users::AssignmentsPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def index?
    user.can_any? :assignments_index, :superadmin
  end

  def assignment?
    user.can_any? :assignments_assignments, :superadmin
  end
end
