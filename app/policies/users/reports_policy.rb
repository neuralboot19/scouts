class Users::ReportsPolicy < ApplicationPolicy
  class Scope < Scope
    # NOTE: Be explicit about which records you allow access to!
    # def resolve
    #   scope.all
    # end
  end

  def reports_nav?
    user.can_any? :reports_nav, :superadmin
  end

  def payments?
    user.can_any? :reports_payments, :superadmin
  end

  def members?
    user.can_any? :reports_members, :superadmin
  end

  def tutors_legals?
    user.can_any? :reports_tutors_legals, :superadmin
  end

  def reports_by_payments?
    user.can_any? :reports_reports_by_payments, :superadmin
  end

  def reports_by_members?
    user.can_any? :reports_reports_by_members, :superadmin
  end

  def reports_by_tutors_legals?
    user.can_any? :reports_reports_by_tutors_legals, :superadmin
  end

  def numerical_cutoff?
    user.can_any? :reports_numerical_cutoff, :superadmin
  end

  def reports_by_numerical_cutoff?
    user.can_any? :reports_reports_by_numerical_cutoff, :superadmin
  end

  def my_group?
    user.can_any? :reports_my_group, :superadmin
  end

  def reports_by_my_group?
    user.can_any? :reports_reports_by_my_group, :superadmin
  end

  def my_district?
    user.can_any? :reports_my_district, :superadmin
  end

  def reports_by_my_district?
    user.can_any? :reports_reports_by_my_district, :superadmin
  end

  def my_region?
    user.can_any? :reports_my_region, :superadmin
  end

  def reports_by_my_region?
    user.can_any? :reports_reports_by_my_region, :superadmin
  end
end
