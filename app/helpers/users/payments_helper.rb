module Users::PaymentsHelper

  def get_card_show_amount(amount)
    if amount.amount_type == "tdc"
      "#{amount.amount_usd} USD"
    else
      if @validate_discount
        amount_descount = amount.amount_usd - 2
        "#{amount_descount * amount.amount_exchange} Bs"
      else
        "#{amount.amount_usd * amount.amount_exchange} Bs"
      end
    end
  end

  def render_views(resource)
    if resource.amount_type == "tdc"
      render 'pay_tdc'
    elsif resource.amount_type == "movil"
      render 'pay_movil'
    elsif resource.amount_type == "manual"
      render 'pay_manual'
    end
  end
end
