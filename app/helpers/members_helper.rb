module MembersHelper
  def login_style(title = '', sub_title = '', image = '', body_styles = '', &block)
    content_tag(:section, class: 'h-100 fbg_indigo') do
      content_tag(:div, class: 'container py-5 h-100') do
        content_tag(:div, class: 'row d-flex justify-content-center align-items-center h-100') do
          content_tag(:div, class: 'col col-xl-10') do
            content_tag(:div, class: 'card', style: 'border-radius: 1rem; background-color: #cac9c9') do
              content_tag(:div,
                          class: 'row g-0  p-0 d-flex justify-content-center align-items-center align-content-center') do
                order_show(title, sub_title, image, body_styles, &block)
              end
            end
          end
        end
      end
    end
  end

  def group_show(cedula)
    info_user = Member.find_by_cedula(cedula)

    if info_user
      if info_user.group_unit?
        render(partial: 'members/group_info', locals: { info_user: })
      else
        render('members/group_form', locals: { info_user: })
      end
    else
      render('members/group_form', locals: { info_user: })
    end
  end

  private

  def order_show(title, sub_title, order, body_styles, &block)
    case order
    when 'rigth'
      concat(build_body(title, sub_title, body_styles, &block))
      concat(build_image)
    when 'left'
      concat(build_image)
      concat(build_body(title, sub_title, body_styles, &block))
    when 'hide'
      concat(build_body(title, sub_title, body_styles, &block))
    else
      concat(build_body(title, sub_title, body_styles, &block))
      concat(build_image)
    end
  end

  def build_image
    content_tag(:div, class: 'col-md-6 col-lg-5 d-none d-md-block h-100') do
      image_tag('logo.png', alt: 'logo', class: 'img-fluid w-100 h-100',
                            style: 'border-radius: 0 1rem 1rem 0;')
    end
  end

  def build_body(title, sub_title, body_styles, &block)
    clases = "card-body p-4 p-lg-5 text-black #{body_styles}".strip
    content_tag(:div, class: 'col-md-6 col-lg-7 d-flex align-items-center flex-column card_body_style') do
      concat(content_tag(:div, class: 'd-flex align-items-center mb-3 pb-1') do
        content_tag(:h1, title, class: 'fw-bold mb-0 mt-5')
      end)
      concat(content_tag(:h5, sub_title, class: 'fw-normal mb-2 pb-3', style: 'letter-spacing: 1px;'))
      concat(content_tag(:div, class: clases) do
        capture(&block)
      end)
    end
  end
end
