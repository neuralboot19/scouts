module FormHelper
  # Genera un campo de texto con los estilos necesarios
  def form_text_field(ref, field_name, value, model_ref, prompt = '', data = {})
    prompt = field_name.humanized if prompt == ''
    content_tag(:div, class: 'field form-outline mb-4') do
      concat(ref.label(field_name, prompt, class: 'form_label'))
      concat(ref.text_field(field_name, autofocus: true,
                                        class: 'form-control form-control-lg', value:, data:))

      concat(warning_message(model_ref, field_name))
    end
  end

  # Genera texto limpio para alertas de validacion
  def warning_message(ref, field)
    content_tag(:div) do
      ref&.errors&.full_messages_for(field.to_sym)&.each do |message|
        clean_msm = message.gsub(/#{field.to_s.gsub('_', ' ')}/i, '')
        concat(content_tag(:div, clean_msm, class: 'text-danger'))
      end
    end
  end

  # el handle value evita escribir tanto if en los values
  def h_value(member, field_name)
    if member.nil?
      nil
    else
      member[field_name]
    end
  end
end
