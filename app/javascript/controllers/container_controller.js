import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="container"
export default class extends Controller {
  connect() {
    this.hideToastAfterDelay();
  }
  hideToastAfterDelay() {
    setTimeout(function () {
      var toast = document.getElementById("liveToast");
      if (toast) {
        toast.classList.add("hide");
        setTimeout(function () {
          toast.parentNode.removeChild(toast);
        }, 500); // Delay para permitir la animación de cierre
      }
    }, 5000); // 5000 ms = 5 segundos
  }
}