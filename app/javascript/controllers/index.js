// This file is auto-generated by ./bin/rails stimulus:manifest:update
// Run that command whenever you add a new controller or create them with
// ./bin/rails generate stimulus controllerName

import { application } from "./application"

import ContainerController from "./container_controller"
application.register("container", ContainerController)

import HelloController from "./hello_controller"
application.register("hello", HelloController)

import PaymentController from "./payment_controller"
application.register("payment", PaymentController)

import RegionController from "./region_controller"
application.register("region", RegionController)

import ValidationsController from "./validations_controller"
application.register("validations", ValidationsController)
