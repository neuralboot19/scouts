import { Controller } from "@hotwired/stimulus";
import { get } from "@rails/request.js";

// Connects to data-controller="region"
export default class extends Controller {
  static targets = ["regionSelect", "districtSelect", "groupSelect", "unitSelect", "advanceSelect", "provinceSelect", "parishSelect"];

  changeRegion(event) {
    let region = event.target.selectedOptions[0].value;
    let target_d = this.districtSelectTarget.id;
    let target_g = this.groupSelectTarget.id;
    let target_u = this.unitSelectTarget.id;

    get(
      `/members/districts?target_d=${target_d}&region=${region}&target_g=${target_g}&target_u=${target_u}`,
      {
        responseKind: "turbo-stream",
      }
    );
  }

  changeDistrict(event) {
    let district = event.target.selectedOptions[0].value;
    let target_g = this.groupSelectTarget.id;
    let target_u = this.unitSelectTarget.id;

    get(`/members/groups?target_g=${target_g}&district=${district}&target_u=${target_u}`, {
      responseKind: "turbo-stream",
    });
  }

  changeGroup(event) {
    let group = event.target.selectedOptions[0].value;
    let target_u = this.unitSelectTarget.id;

    get(`/members/units?target_u=${target_u}&group=${group}`, {
      responseKind: "turbo-stream",
    });
  }

  changeUnit(event) {
    let unit = event.target.selectedOptions[0].value;
    let advanceA = this.advanceSelectTarget.id;

    get(
      `/members/advance?unit=${unit}&advanceA=${advanceA}`,
      {
        responseKind: "turbo-stream",
      }
    );
  }

  changeState(event) {
    let state = event.target.selectedOptions[0].value;
    let target_p = this.provinceSelectTarget.id;
    let target_h = this.parishSelectTarget.id;

    get(
      `/members/states?state=${state}&target_p=${target_p}&target_h=${target_h}`,
      {
        responseKind: "turbo-stream",
      }
    );
  }

  changeProvince(event) {
    let province = event.target.selectedOptions[0].value;
    let target_h = this.parishSelectTarget.id;

    get(
      `/members/provinces?province=${province}&target_h=${target_h}`,
      {
        responseKind: "turbo-stream",
      }
    );
  }
}
