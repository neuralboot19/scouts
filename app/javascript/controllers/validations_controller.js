import { Controller } from "@hotwired/stimulus";

// Connects to data-controller="validations"
export default class extends Controller {
  static targets = ["cedulaField", "errorMessage"];

  filterNumeric(event) {
    const input = event.target;
    input.value = input.value.replace(/[^0-9]/g, "");
  }
  cedula(event) {
    const fieldValue = this.cedulaFieldTarget.value;
    const errorMessage = this.errorMessageTarget;

    // Valida que tenga entre 7 a 8 digitos la cedula
    if (fieldValue.length < 7 || fieldValue.length > 15) {
      errorMessage.textContent = "Se requieren de 7 a 15 caracteres.";
      errorMessage.style.display = "block";
      event.preventDefault();
    }
  }
}
