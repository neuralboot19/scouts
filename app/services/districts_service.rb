# app/services/districts_service.rb
class DistrictsService
  def initialize(params)
    @params = params
  end

  def perform
    authorize_user
    prepare_data
  end

  private

  def authorize_user
    # Tu lógica de autorización aquí
  end

  def prepare_data
    @target_d = @params[:target_d]
    @target_g = @params[:target_g]
    @region = Region.find_by(region_name: @params[:region])
    @districts = ['Selecciona un distrito']
    @groups = ['Selecciona un grupo']
    @districts.concat(@region.districts.pluck(:district_name)) unless @region.nil?
    # No uses respond_to en un servicio, solo prepara los datos necesarios y devuelve lo que sea necesario.
    # En este caso, puedes devolver los datos que se están preparando.
    { target_d: @target_d, target_g: @target_g, districts: @districts, groups: @groups }
  end
end
