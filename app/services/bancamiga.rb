class Bancamiga
  def self.list_banks(api)
    conn = Connection.prepare_connection(api, '/public/protected/p2c/banks')
    Connection.get_request(conn, api.api_token)
  end

  def self.find_payment(api, body)
    conn = Connection.prepare_connection(api, '/public/protected/pm/find')
    Connection.post_request(conn, api.api_token, body)
  end
end
