class PaymentMailer < ApplicationMailer
  def processed_email(user)
    @user = user
    mail(to: @user.email, subject: 'Notificación de pago en proceso')
  end

  def declined_email(user)
    @user = user
    mail(to: @user.email, subject: 'Notificación de pago declinado')
  end

  def accepted_email(user)
    @user = user
    mail(to: @user.email, subject: 'Notificación de pago aceptado')
  end

  def expiration_payment_email(user)
    @user = user
    mail(to: @user.email, subject: 'Notificación de expiración')
  end
end
