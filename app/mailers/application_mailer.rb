class ApplicationMailer < ActionMailer::Base
  default from: "noreply@scouts.org.ve"
  layout "mailer"
end
