class Allergy < ApplicationRecord
  has_many :allergy_members
  has_many :members, through: :allergy_members

  validates :allergies_name, presence: true

  scope :active, -> { where(allergies_status: true) }
  scope :inactive, -> { where(allergies_status: false) }
end
