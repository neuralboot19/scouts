class Handicap < ApplicationRecord
  has_many :handicap_members
  has_many :members, through: :handicap_members

  validates :handicaps_name, presence: true

  scope :active, -> { where(handicaps_status: true) }
  scope :inactive, -> { where(handicaps_status: false) }
end
