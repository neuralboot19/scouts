class Role < ApplicationRecord
  has_many :users
  has_many :role_permissions, dependent: :destroy
  has_many :permissions, through: :role_permissions

  validates :rol_name, presence: true, uniqueness: { case_sensitive: false }

  def can_any?(*permission_types)
    permissions.where(permission_type: permission_types).any?
  end
end
