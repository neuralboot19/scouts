class Vaccine < ApplicationRecord
  has_many :vaccine_members
  has_many :members, through: :vaccine_members

  validates :vaccine_name, presence: true

  scope :active, -> { where(vaccine_status: true) }
  scope :inactive, -> { where(vaccine_status: false) }
end
