class Group < ApplicationRecord
  belongs_to :district
  has_many :group_units
end
