class CorporateMember < ApplicationRecord
  belongs_to :member
  belongs_to :corporate_jobtitle
  belongs_to :region
  belongs_to :district
end
