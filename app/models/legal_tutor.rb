class LegalTutor < ApplicationRecord
  # Asociaciones
  belongs_to :member, required: false
  belongs_to :state, required: false
  belongs_to :province, required: false
  belongs_to :parish, required: false
  belongs_to :religion, required: false
  
  # Validaciones
  validates :cedula, length: { minimum: 7, maximum: 15 }
  validates :age, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 150 }

  # Enums
  enum ocupation_type: { publico: 0, privado: 1 }
  enum citizenship: { V: '0', E: '1' }
  enum level_education: { Primaria: 0, Secundaria: 1, Superior: 2 }
  
  # Método para calcular la integridad del tutor legal
  def member_completeness
    present_fields = %i[cedula gender first_name last_name age birthdate second_email state province parishe address]
    total_fields = present_fields.count
    total_present = present_fields.count { |field| self[field].present? }
    (total_present.to_f / total_fields) * 100
  end
end
