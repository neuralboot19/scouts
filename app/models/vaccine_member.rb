class VaccineMember < ApplicationRecord
	belongs_to :member
  belongs_to :vaccine

  scope :active, -> { where(vaccine_m_status: true) }

end
