class GroupUnit < ApplicationRecord
  belongs_to :unit
  belongs_to :group
  has_many :members

  enum group_unit_status: { active: 0, inactive: 1 }
end
