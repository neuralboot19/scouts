class IntoleranceMember < ApplicationRecord
  belongs_to :member
  belongs_to :intolerance

  scope :active, -> { where(intolerance_m_status: true) }
end
