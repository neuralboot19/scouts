class Payment < ApplicationRecord
  belongs_to :user

  enum payment_status: { pendiente: 0, activo: 1, inactivo: 2 }
end
