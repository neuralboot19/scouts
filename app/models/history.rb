class History < ApplicationRecord
  has_many :history_members
  has_many :members, through: :history_members

  validates :histories_name, presence: true

  scope :active, -> { where(histories_status: true) }
  scope :inactive, -> { where(histories_status: false) }
end
