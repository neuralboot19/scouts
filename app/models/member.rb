class Member < ApplicationRecord
  # Asociaciones
  belongs_to :group_unit, required: false
  belongs_to :state, required: false
  belongs_to :province, required: false
  belongs_to :parish, required: false
  belongs_to :religion, required: false
  has_many :users, dependent: :destroy
  has_many :legal_tutors
  has_many :jobtitle_members
  has_many :medical_files
  has_many :vaccine_members
  has_many :vaccines, through: :vaccine_members
  has_many :allergy_members
  has_many :allergy, through: :allergy_members
  has_many :disease_members
  has_many :disease, through: :disease_members
  has_many :history_members
  has_many :history, through: :history_members
  has_many :handicap_members
  has_many :handicap, through: :handicap_members
  has_many :intolerance_members
  has_many :intolerance, through: :intolerance_members

  # Validaciones
  validates :cedula, length: { minimum: 7, maximum: 15 }, uniqueness: true
  validates :age, numericality: { only_integer: true, greater_than_or_equal_to: 0, less_than_or_equal_to: 150 }

  # Enums
  enum member_type: { grupo: 0, dualidad: 1, institucional: 2 }
  enum is_adult: { adult: 0, youth: 1 }
  enum ocupation_type: { publico: 0, privado: 1 }
  enum status_renewal: { enabled: 0, disable: 1 }
  enum citizenship: { V: 0, E: 1 }
  enum level_education: { Primaria: 0, Secundaria: 1, Superior: 2 }

  before_update :validate_completeness

  # Método para calcular la integridad del miembro
  def member_completeness
    present_fields = %i[cedula gender first_name last_name age birthdate date_admission date_promise second_email state province parish address]
    total_fields = present_fields.count
    total_present = present_fields.count { |field| send(field).present? }
    (total_present.to_f / total_fields) * 100
  end

  private

  def validate_completeness
    users.last.update(completeness: member_completeness.round(2))
  end
end
