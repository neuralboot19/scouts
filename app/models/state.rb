class State < ApplicationRecord
  belongs_to :country
  has_many :provinces
  has_many :members

end
