class District < ApplicationRecord
  belongs_to :region
  has_many :groups
  has_many :corporate_members
end
