class Unit < ApplicationRecord
  has_many :group_units
  has_many :advances
end
