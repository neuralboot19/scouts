class MedicalFile < ApplicationRecord
  belongs_to :member

  validates :blood_type, presence: true
  validates :height, presence: true, numericality: { only_integer: true, less_than_or_equal_to: 999, message: I18n.t('errors.messages.three_digits_max') }
  validates :weight, presence: true, numericality: { only_integer: true, less_than_or_equal_to: 999, message: I18n.t('errors.messages.three_digits_max') }
  validates :insurance_name, :emergency_first_name, :emergency_last_name, presence: true, format: { with: /\A[a-zA-Z]+\z/, message: I18n.t('errors.messages.only_letters') }
  validates :emergency_number, presence: true
  validates :emergency_number, format: { with: /\A\d+\z/, message: I18n.t('errors.messages.only_numbers') },
                               length: { is: 10, message: I18n.t('errors.messages.ten_digits') }
end
