class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable, :confirmable

  belongs_to :role, optional: true
  belongs_to :member, optional: true, dependent: :destroy

  has_many :payments

  enum status: { pendiente: 0, activo: 1, inactivo: 2 }

  validate :user_completeness, on: %i[create update]

  def can_any?(*)
    role ? role.can_any?(*) : false
  end

  def self.status_payments?
    set_status
  end

  def status_payments?
    set_status
  end

  def payment_is_validate?
    search_pay = payments.where(payment_status: 'activo').last
    date_renewal_end = Date.today <= search_pay.renewal_end if search_pay.present?
    return true if date_renewal_end

    false
  end

  def payment_renewal_end
    search_pay = payments.where(payment_status: 'activo').last
    search_pay&.renewal_end
  end

  def full_name
    member&.first_name + " " + member&.middle_name + " " + member&.last_name
  end

  private

  def user_completeness
    self.completeness = member.member_completeness.round(2) if member.present?
  end

  def set_status
    return true if payments.present? && status == 'activo'

    false
  end
end
