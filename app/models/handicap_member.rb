class HandicapMember < ApplicationRecord
  belongs_to :member
  belongs_to :handicap

  scope :active, -> { where(handicap_m_status: true) }
end
