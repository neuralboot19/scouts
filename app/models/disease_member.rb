class DiseaseMember < ApplicationRecord
  belongs_to :member
  belongs_to :disease

  scope :active, -> { where(disease_m_status: true) }
end
