class Intolerance < ApplicationRecord
  has_many :intolerance_members
  has_many :members, through: :intolerance_members

  validates :intolerances_name, presence: true

  scope :active, -> { where(intolerances_status: true) }
  scope :inactive, -> { where(intolerances_status: false) }
end
