class TrainingOpportunity < ApplicationRecord
  belongs_to :training
  belongs_to :opportunity
end
