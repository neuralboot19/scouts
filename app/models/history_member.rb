class HistoryMember < ApplicationRecord
  belongs_to :member
  belongs_to :history

  scope :active, -> { where(history_m_status: true) }
end
