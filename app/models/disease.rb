class Disease < ApplicationRecord
  has_many :disease_members
  has_many :members, through: :disease_members

  validates :diseases_name, presence: true

  scope :active, -> { where(diseases_status: true) }
  scope :inactive, -> { where(diseases_status: false) }
end
