class Region < ApplicationRecord
  belongs_to :country
  has_many :districts
  has_many :corporate_members
end
