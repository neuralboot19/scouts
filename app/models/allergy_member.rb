class AllergyMember < ApplicationRecord
  belongs_to :member
  belongs_to :allergy

  scope :active, -> { where(allergy_m_status: true) }
end
