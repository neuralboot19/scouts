class Province < ApplicationRecord
  belongs_to :state
  has_many :parishes
  has_many :members

end
