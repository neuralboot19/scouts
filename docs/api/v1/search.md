# Api::V1::HomeController

Este controlador maneja las acciones relacionadas con el API en la versión 1.

## Acciones

### `search`

- **Descripción:** Busca un usuario por correo electrónico y retorna información del usuario y su status.

  - **Método HTTP:** POST
  - **Ruta:** `/api/v1/home/search`
  - **Parámetros:**
    - `email` (cadena): Correo electrónico del usuario que se desea buscar.

  - **Respuestas:**
    - **Éxito (200 OK):** Se encuentra el usuario y se devuelve la información.
      ```json
      {
        "status": "activo",
        "cedula": "123456789",
        "email": "usuario@dominio.com"
      }
      ```
    - **Error (404 Not Found):** No se encuentra el usuario.
      ```json
      {
        "error": "Usuario no encontrado"
      }
      ```

## Consideraciones de Seguridad

- La acción `search` está configurada para permitir acceso sin autenticación ni autorización.
